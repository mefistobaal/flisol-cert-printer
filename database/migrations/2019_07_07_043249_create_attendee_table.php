<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendee', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_names');
            $table->string('second_names');
            $table->enum('doc_type', ['CC', 'Passport', 'TI', 'NIT']);
            $table->unsignedBigInteger('document')->unique();
            $table->string('email', 60);
            $table->string('phone');
            $table->boolean('status')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_attendee');
    }
}
