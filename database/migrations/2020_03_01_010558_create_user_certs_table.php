<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_certs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('attendee_id');
            $table->unsignedBigInteger('cert_id');
            $table->unsignedBigInteger('rol_id');
            $table->string('verification_code')->unique();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('user_certs', function (Blueprint $table) {
            $table->foreign('attendee_id')->references('id')->on('attendee')->onDelete('cascade');
            $table->foreign('cert_id')->references('id')->on('cert')->onDelete('cascade');
            $table->foreign('rol_id')->references('id')->on('rol')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_certs');
    }
}
