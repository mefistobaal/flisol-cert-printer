<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cert', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cert_title');
            $table->string('cert_title_color')->nullable();
            $table->string('celebrated_on');
            $table->date('year');
            $table->string('year_color')->nullable();
            $table->string('name_color')->nullable();
            $table->string('cert_image_path')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certs');
    }
}
