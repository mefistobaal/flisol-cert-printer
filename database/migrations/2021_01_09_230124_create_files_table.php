<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('route');
            $table->timestamps();
        });
        Schema::table('system_configs', function (Blueprint $table) {
            $table->foreign('favicon')->references('id')->on('files')->onDelete('cascade');
            $table->foreign('home_logo')->references('id')->on('files')->onDelete('cascade');
            $table->foreign('admin_logo')->references('id')->on('files')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
