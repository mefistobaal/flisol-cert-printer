<?php

namespace Database\Seeders;

use App\Models\CourseCert;
use Illuminate\Database\Seeder;

class CourseCertSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CourseCert::create([
            'cert_id'   => 1,
            'course_id' => 1,
        ]);
        CourseCert::create([
            'cert_id'   => 2,
            'course_id' => 1,
        ]);
        CourseCert::create([
            'cert_id'   => 3,
            'course_id' => 1,
        ]);
        CourseCert::create([
            'cert_id'   => 4,
            'course_id' => 1,
        ]);
    }
}
