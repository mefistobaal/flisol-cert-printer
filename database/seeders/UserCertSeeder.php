<?php

namespace Database\Seeders;

use App\Models\UserCert;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserCertSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserCert::create([
            'attendee_id'        => 1,
            'cert_id'            => 1,
            'rol_id'             => 1,
            'verification_code' => Str::uuid()
        ]);
        UserCert::create([
            'attendee_id'        => 1,
            'cert_id'            => 3,
            'rol_id'             => 1,
            'verification_code' => Str::uuid()
        ]);
    }
}
