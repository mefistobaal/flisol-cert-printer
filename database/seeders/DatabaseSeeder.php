<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdminSeeder::class,
            CourseSeeder::class,
            CertSeeder::class,
            CourseCertSeeder::class,
            RolSeeder::class,
            AttendeeSeeder::class,
            UserCertSeeder::class,
            StatsSeeder::class,
            SystemConfigsSeeder::class
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
