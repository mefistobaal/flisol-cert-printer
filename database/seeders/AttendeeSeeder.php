<?php

namespace Database\Seeders;

use App\Models\Attendee;
use Illuminate\Database\Seeder;

class AttendeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Attendee::create([
            'first_names'  => 'Santiago',
            'second_names' => 'Hurtado',
            'doc_type'     => 'CC',
            'document'     => 1057601581,
            'email'        => 'dasanti_12@hotmail.com',
            'phone'        => 3200000000
        ]);
    }
}
