<?php

namespace Database\Seeders;

use App\Models\Cert;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class CertSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cert::create([
            'cert_title'    => 'El fortalecimiento de la radio comunitaria en Colombia',
            'celebrated_on' => 'virtual',
            'year' => Carbon::now()
        ]);
        Cert::create([
            'cert_title'    => '¿Cómo conducir una entrevista en vivo? ',
            'celebrated_on' => 'virtual',
            'year' => Carbon::now()
        ]);
        Cert::create([
            'cert_title'    => 'El poder de la voz',
            'celebrated_on' => 'virtual',
            'year' => Carbon::now()
        ]);
        Cert::create([
            'cert_title'    => 'Derechos, propiedad sobre radiodifusión',
            'celebrated_on' => 'virtual',
            'year' => Carbon::now()
        ]);
    }
}
