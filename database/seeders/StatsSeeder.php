<?php

namespace Database\Seeders;

use App\Models\Stats;
use Illuminate\Database\Seeder;

class StatsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Stats::create(
            [
                'certs_gen'    => 0,
                'certs_sended' => 0,
                'querys_gen'   => 0
            ]
        );
    }
}
