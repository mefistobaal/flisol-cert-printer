<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AttendeeController;
use App\Http\Controllers\Admin\CoursesController;
use App\Http\Controllers\Admin\SessionController;
use App\Http\Controllers\Cert\CertController;
use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\SystemController;
use App\Models\Admin;
use App\Models\Attendee;
use App\Models\Cert;
use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->group(function () {
    Route::get('/config', [SystemController::class, 'system']);
    Route::post('/login', [SessionController::class, 'login']);
    Route::post('/certificado', [HomeController::class, 'query']);
});
/* Auth GET */
Route::middleware('auth:api')->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::get('/certs/{id?}', function (Cert $cert, $id = null) {
        if ($id) {
            return $cert->find($id)->load('course');
        } else {
            return $cert->load('attendees')->paginate();
        }
    });
    Route::get('/stats', [HomeController::class, 'stats']);
    Route::get('/admins/{id?}', function (Admin $admin, $id = null) {
        if ($id) {
            return $admin->find($id);
        } else {
            return $admin->all();
        }
    });
    Route::get('/attendees', function (Attendee $attendee) {
        return $attendee->paginate();
    });
    Route::get('/attendees/{id}', function (Attendee $attendee, $id) {
        return $attendee->withTrashed()->find($id);
    });
    Route::get('/attendee/export', [AttendeeController::class, 'export']);
    Route::get('/courses/{id?}', function (Course $course, $id = null) {
        if ($id) {
            return $course->find($id)->load('certs');
        } else {
            return $course->all()->load('certs');
        }
    });
});
/* Auth Post */
Route::middleware('auth:api')->group(function () {
    Route::post('/config', [SystemController::class, 'update']);
    Route::post('/certs', [CertController::class, 'search']);
    Route::post('/certs/create', [CertController::class, 'create']);
    Route::post('/certs/assign', [CertController::class, 'assign']);
    Route::post('/admins', [AdminController::class, 'create']);
    Route::post('/attendee', [AttendeeController::class, 'create']);
    Route::post('/attendees', [AttendeeController::class, 'search']);
    Route::post('/attendee/import', [AttendeeController::class, 'import']);
    Route::post('/courses', [CoursesController::class, 'create']);
});
/* Auth PUT */
Route::middleware('auth:api')->group(function () {

});
/* Auth PATCH*/
Route::middleware('auth:api')->group(function () {
    Route::patch('/attendee', [AttendeeController::class, 'update']);
    Route::patch('/attendee/restore/{id}', [AttendeeController::class, 'restore']);
    Route::patch('/attendee/certs', [AttendeeController::class, 'certs']);
    Route::patch('/certs', [CertController::class, 'update']);
    Route::patch('/course', [CoursesController::class, 'update']);
    Route::patch('/admins', [AdminController::class, 'update']);
});

/* Auth DELETE*/
Route::middleware('auth:api')->group(function () {
    Route::delete('/attendee/{id}', [AttendeeController::class, 'delete']);
    Route::delete('/certs/{id}', [CertController::class, 'delete']);
    Route::delete('/admins/{id}', [AdminController::class, 'delete']);
});
