const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.browserSync('127.0.0.1:8080')
mix.js('resources/js/app.js', 'public/js').version();
mix.react('resources/js/home/app.jsx', 'public/js/react/home').version().sourceMaps()
    .react('resources/js/login/app.jsx', 'public/js/react/login').version().sourceMaps()
    .react('resources/js/admin/app.jsx', 'public/js/react/admin').version().sourceMaps();
mix.copy('resources/js/main.js', 'public/js/main.js')
