<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Certificado de asistencia</title>
    <style type="text/css">
        * {
            font-family: Arial, sans-serif;
            text-align: center;
            margin: 0;

        }

        body {
            background-image: url({{$certImagePath}});
            background-repeat: no-repeat;
            background-size: contain;
            align-content: center;
        }

        .name {
            text-align: center;
            margin-top: 42%;
            margin-left: 50px;
            margin-right: 50px;
            line-height: 65px;
            color: {{$nameColor ?? 'gray'}};
            font-size: 70px;
        }

        .cc {
            margin-top: 10px;
            text-align: center;
            font-size: 30px;
            color: deeppink;
        }

        .taller {
            text-align: center;
            margin-top: 10px;
            font-size: 25px;
            color: {{$titleColor ?? '#2858be'}};
        }

        .site {
            font-size: 16px;
            text-align: center;
            margin-top: 5px;
            color: {{$dateColor ?? '#2858be'}};
        }
    </style>
</head>
<body>
<div>
    <h1 class="name">{{$name}}</h1>
    <h1 class="cc">{{$document}}</h1>
    <h1 class="taller">{{$title}}</h1>
    <h1 class="site">Realizado el {{$date ?? ''}}</h1>
</div>
</body>
</html>
