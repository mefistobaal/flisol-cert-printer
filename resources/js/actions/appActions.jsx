import axios from 'axios';

let token = sessionStorage.getItem('certToken')

export const VALIDATE_TOKEN = () => async dispatch => {
    if (token == null) {
        location.replace('/login')
    }
}

export const CLOSE_SESSION = () => async dispatch => {
    sessionStorage.removeItem('certToken')
    location.replace('/login')
}

export const GET_USER_DATA = () => async dispatch => {
    const User = async () => {
        try {
            const {data} = await axios.get('/api/user', {
                headers: {
                    'Authorization': token
                }
            });
            dispatch({
                type: 'GET_USER_DATA',
                payload: data
            })
        } catch (e) {
            sessionStorage.clear()
            location.replace('/login')
        }
    }
    User()
    setInterval(async () => {
        await User()
    }, 60000)
};

export const GET_ACTUAL_CONFIG = () => async dispatch => {
    const {data} = await axios.get('/api/config')
    if (data.admin_logo === null) {
        data.admin_logo = "images/icon/logo-white.png"
    }
    dispatch({
        type: 'GET_ACTUAL_CONFIG',
        payload: data
    })
};

export const SET_ACTUAL_CONFIG = dataR => async dispatch => {
    let request = new FormData()
    request.append('site_title', dataR.site_title)
    request.append('favicon', dataR.favicon[0])
    request.append('admin_logo', dataR.admin_logo[0])
    request.append('home_logo', dataR.home_logo[0])
    try {
        const {data} = await axios.post('/api/config', request, {
            headers: {
                Authorization: token
            }
        })
        dispatch({
            type: 'SAVE_ACTUAL_CONFIG',
            payload: {
                saved: true,
                loading: false,
                error: false,
                message: {
                    message: '',
                    errors: []
                }
            }
        })
    } catch (e) {
        if (!e.response.data.hasOwnProperty('errors')) {
            Object.defineProperty(e.response.data, 'errors', {
                value: [],
                enumerable: true
            })
        }
        dispatch({
            type: 'SAVE_ACTUAL_CONFIG',
            payload: {
                error: true,
                message: e.response.data
            }
        })
    }
};
