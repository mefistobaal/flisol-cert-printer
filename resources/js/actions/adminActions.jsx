import axios from "axios";

const token = sessionStorage.getItem('certToken')

export const GET_ADMINS = () => async dispatch => {
    try {
        const {data} = await axios.get('/api/admins', {
            headers: {
                Authorization: token
            }
        })
        dispatch({
            type: 'GET_ADMINS',
            payload: data
        })
    } catch (e) {

    }
}

export const SAVE_NEW_ADMIN = dataF => async dispatch => {
    dispatch({
        type: 'SAVE_NEW_ADMIN',
        payload: {
            created: false,
            loading: true,
            error: false,
            message: {message: '', errors: []}
        }
    })
    try {
        const {data} = await axios.post('/api/admins', dataF, {
            headers: {
                Authorization: token
            }
        })
        dispatch({
            type: 'SAVE_NEW_ADMIN',
            payload: {
                created: true,
                loading: false,
                error: false,
                message: {message: '', errors: []}
            }
        })
    } catch (e) {
        if (!e.response.data.hasOwnProperty('errors')) {
            Object.defineProperty(e.response.data, 'errors', {
                value: [],
                enumerable: true
            })
        }
        dispatch({
            type: 'SAVE_NEW_ADMIN',
            payload: {
                created: false,
                loading: false,
                error: true,
                message: e.response.data
            }
        })
    }
}

export const GET_EDIT_ADMIN = id => async dispatch => {
    try {
        const {data} = await axios.get('/api/admins/' + id, {
            headers: {Authorization: token}
        })
        dispatch({
            type: 'GET_ADMIN_EDIT',
            payload: data
        })
    } catch (e) {
        if (!e.response.data.hasOwnProperty('errors')) {
            Object.defineProperty(e.response.data, 'errors', {
                value: [],
                enumerable: true
            })
        }
        dispatch({
            type: 'SAVE_NEW_ADMIN',
            payload: {
                created: false,
                loading: false,
                error: true,
                message: e.response.data
            }
        })
    }
}

export const SAVE_EDIT_ADMIN = dataF => async dispatch => {
    dispatch({
        type: 'SAVE_NEW_ADMIN',
        payload: {
            created: false,
            loading: true,
            error: false,
            message: {message: '', errors: []}
        }
    })
    try {
        const {data} = await axios.patch('/api/admins', dataF, {
            headers: {
                Authorization: token
            }
        })
        dispatch({
            type: 'SAVE_NEW_ADMIN',
            payload: {
                created: true,
                loading: false,
                error: false,
                message: {message: '', errors: []}
            }
        })
    } catch (e) {
        if (!e.response.data.hasOwnProperty('errors')) {
            Object.defineProperty(e.response.data, 'errors', {
                value: [],
                enumerable: true
            })
        }
        dispatch({
            type: 'SAVE_NEW_ADMIN',
            payload: {
                created: false,
                loading: false,
                error: true,
                message: e.response.data
            }
        })
    }
}

export const DELETE_ADMIN = id => async dispatch => {
    const {data} = await axios.delete('/api/admins/' + id, {
        headers: {Authorization: token}
    })
}
