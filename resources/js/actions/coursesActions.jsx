import axios from "axios";

const token = sessionStorage.getItem('certToken')

export const GET_COURSES = () => async dispatch => {
    try {
        const {data} = await axios.get('/api/courses', {
            headers: {
                Authorization: token
            }
        })
        dispatch({
            type: 'GET_COURSES',
            payload: data
        })
    } catch (e) {
        dispatch({
            type: 'GET_COURSES',
            payload: {error: true, message: e.response.data}
        })
    }
}

export const COURSE_CREATE = dataF => async dispatch => {
    try {
        const {data} = await axios.post('/api/courses', dataF, {
            headers: {
                Authorization: token
            }
        })
        dispatch({
            type: 'COURSE_CREATE',
            payload: {error: false, message: {message: '', errors: {}}, created: true}
        })
    } catch (e) {
        if (!e.response.data.hasOwnProperty('errors')) {
            Object.defineProperty(e.response.data, 'errors', {
                enumerable: true,
                value: {}
            })
        }
        dispatch({
            type: 'COURSE_CREATE',
            payload: {error: true, message: e.response.data, created: false}
        })
    }
}

export const GET_COURSE_EDIT = id => async dispatch => {
    try {
        const {data} = await axios.get('/api/courses/' + id, {
            headers: {
                Authorization: token
            }
        })
        dispatch({
            type: 'GET_COURSE_EDIT',
            payload: {
                loaded: true,
                course: data
            }
        })
    } catch (e) {

    }
}

export const EDIT_COURSE = dataF => async dispatch => {
    try {
        dispatch({
            type: 'EDIT_COURSE',
            payload: {created: false, error: false, message: {message: '', errors: {}}, loading: true}
        })
        const {data} = await axios.patch('/api/course', dataF, {
            headers: {
                Authorization: token
            }
        });
        dispatch({
            type: 'EDIT_COURSE',
            payload: {created: true, error: false, message: {message: '', errors: {}}, loading: false}
        })
    } catch (e) {
        dispatch({
            type: 'EDIT_COURSE',
            payload: {created: false, error: true, message: e.response.data, loading: false}
        })
    }
}

export const CLEANUP = courses => async dispatch => {
    dispatch({
        type: 'CLEANUP_COURSE',
        payload: {
            courses,
            error: false,
            message: {message: '', errors: {}},
            imported: false,
            loading: null,
            created: false,
            edit: {
                loaded: false,
                course: {
                    certs: []
                }
            }
        }
    })
}
