import axios from 'axios'

let token = sessionStorage.getItem('certToken')

export const GET_STATS = () => async dispatch => {
    const {data} = await axios.get('/api/stats', {
        headers: {
            'Authorization': token
        }
    })
    dispatch({
        type: 'GET_STATS',
        payload: data
    })
};
