import axios from "axios";

const token = sessionStorage.getItem('certToken')

export const GET_ATTENDEES = () => async dispatch => {
    try {
        const {data} = await axios.get('/api/attendees', {
            headers: {
                Authorization: token
            }
        })
        dispatch({
            type: 'GET_ATTENDEES',
            payload: {attendees: data, error: false, message: {message: '', errors: {}}}
        })
    } catch (e) {
        dispatch({
            type: 'GET_ATTENDEES',
            payload: {
                attendees: [],
                message: e.response.message,
                error: true,
            }
        })
    }
}

export const GET_ATTENDEES_PAGE = url => async dispatch => {
    try {
        const {data} = await axios.get(url, {
            headers: {
                Authorization: token
            }
        })
        dispatch({
            type: 'GET_ATTENDEES',
            payload: {attendees: data, error: false, message: {message: '', errors: {}}}
        })
    } catch (e) {
        dispatch({
            type: 'GET_ATTENDEES',
            payload: {
                attendees: [],
                message: e.response.message,
                error: true,
            }
        })
    }
}

export const IMPORT_ATTENDEES_LIST = file => async dispatch => {
    const fileToSend = new FormData()
    fileToSend.append('excel', file)
    try {
        dispatch({
            type: 'IMPORT_ATTENDEES_LIST',
            payload: {imported: false, error: false, loading: true, message: {message: '', errors: {}}}
        })
        const {data} = await axios.post('/api/attendee/import', fileToSend, {
            headers: {
                Authorization: token
            }
        })
        dispatch({
            type: 'IMPORT_ATTENDEES_LIST',
            payload: {imported: true, error: false, loading: false, message: {message: '', errors: {}}}
        })
    } catch (e) {
        dispatch({
            type: 'IMPORT_ATTENDEES_LIST',
            payload: {imported: false, error: true, message: e.response.data, loading: false}
        })
    }
}


export const EXPORT_ATTENDEES_LIST = () => async dispatch => {
    try {
        const {data} = await axios.get('/api/attendee/export', {
            headers: {
                Authorization: token
            }
        })
        const url = window.URL.createObjectURL(new Blob([data]))
        const a = document.createElement("a")
        a.href = url
        a.download = `asistentes-${new Date().toDateString()}.csv`
        a.click()
    } catch (e) {

    }
}

export const CREATE_ATTENDEE = dataF => async dispatch => {
    try {
        dispatch({
            type: 'CREATE_ATTENDEE',
            payload: {created: false, error: false, message: {message: '', errors: {}}, loading: true}
        })
        const {data} = await axios.post('/api/attendee', dataF, {
            headers: {
                Authorization: token
            }
        });
        dispatch({
            type: 'CREATE_ATTENDEE',
            payload: {created: true, error: false, message: {message: '', errors: {}}, loading: false}
        })
    } catch (e) {
        dispatch({
            type: 'CREATE_ATTENDEE',
            payload: {created: false, error: true, message: e.response.data, loading: false}
        })
    }
}

export const EDIT_ATTENDEE = dataF => async dispatch => {
    try {
        dispatch({
            type: 'EDIT_ATTENDEE',
            payload: {created: false, error: false, message: {message: '', errors: {}}, loading: true}
        })
        const {data} = await axios.patch('/api/attendee', dataF, {
            headers: {
                Authorization: token
            }
        });
        dispatch({
            type: 'EDIT_ATTENDEE',
            payload: {created: true, error: false, message: {message: '', errors: {}}, loading: false}
        })
    } catch (e) {
        dispatch({
            type: 'EDIT_ATTENDEE',
            payload: {created: false, error: true, message: e.response.data, loading: false}
        })
    }
}

export const GET_ATTENDEE_EDIT = id => async dispatch => {
    try {
        const {data} = await axios.get('/api/attendees/' + id, {
            headers: {
                Authorization: token
            }
        })
        dispatch({
            type: 'GET_ATTENDEE_EDIT',
            payload: {
                loaded: true,
                attendee: data
            }
        })
    } catch (e) {

    }
}

export const DELETE_ATTENDEE = id => async dispatch => {
    try {
        dispatch({
            type: 'EDIT_ATTENDEE',
            payload: {created: false, error: false, message: {message: '', errors: {}}, loading: true}
        })
        const {data} = await axios.delete('/api/attendee/' + id, {
            headers: {
                Authorization: token
            }
        });
        dispatch({
            type: 'EDIT_ATTENDEE',
            payload: {created: true, error: false, message: {message: '', errors: {}}, loading: false}
        })
    } catch (e) {
        dispatch({
            type: 'EDIT_ATTENDEE',
            payload: {created: false, error: true, message: e.response.data, loading: false}
        })
    }
}

export const SEARCH_ATTENDEE = (doc_type, document) => async dispatch => {
    document = document == null ? '' : document
    try {
        const {data} = await axios.post('/api/attendees', {doc_type, document}, {
            headers: {
                Authorization: token
            }
        })
        dispatch({
            type: 'GET_ATTENDEES',
            payload: {attendees: data, error: false, message: {message: '', errors: {}}}
        })
    } catch (e) {
        dispatch({
            type: 'GET_ATTENDEES',
            payload: {
                attendees: {
                    data: []
                },
                message: e.response.data,
                error: true,
            }
        })
    }
}

export const ATTENDEE_CERT_ASSIGN = certs => async dispatch => {
    const {data} = await axios.patch('/api/attendee/certs', certs, {
        headers: {
            Authorization: token
        }
    })
    console.log('response', data)
}

export const CLEANUP = attendees => async dispatch => {
    dispatch({
        type: 'CLEANUP',
        payload: {
            attendees,
            error: false,
            message: {message: '', errors: {}},
            imported: false,
            loading: null,
            created: false,
            edit: {
                loaded: false,
                attendee: {
                    certs: []
                }
            }
        }
    })
}

export const RESTORE_ATTENDEE = id => async dispatch => {
    try {
        dispatch({
            type: 'EDIT_ATTENDEE',
            payload: {created: false, error: false, message: {message: '', errors: {}}, loading: true}
        })
        const {data} = await axios.patch('/api/attendee/restore/' + id, {}, {
            headers: {
                Authorization: token
            }
        });
        dispatch({
            type: 'GET_ATTENDEE_EDIT',
            payload: {
                loaded: true,
                attendee: data
            }
        })
    } catch (e) {
        dispatch({
            type: 'EDIT_ATTENDEE',
            payload: {created: false, error: true, message: e.response.data, loading: false}
        })
    }
}
