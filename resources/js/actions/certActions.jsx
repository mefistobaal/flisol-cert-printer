import axios from "axios";

let token = sessionStorage.getItem('certToken')

export const GET_CERTS = () => async dispatch => {
    const {data} = await axios.get('/api/certs', {
        headers: {
            Authorization: token
        }
    })
    dispatch({
        type: 'GET_CERTS',
        payload: data
    })
}

export const GET_CERTS_PAGE = url => async dispatch => {
    const {data} = await axios.get(url, {
        headers: {
            Authorization: token
        }
    })
    dispatch({
        type: 'GET_CERTS',
        payload: data
    })
}

export const SEARCH_CERT = search => async dispatch => {
    const {data} = await axios.post('/api/certs', {search}, {
        headers: {
            Authorization: token
        }
    })
    dispatch({
        type: 'GET_CERTS',
        payload: data
    })
}

export const GET_CERT_EDIT = id => async dispatch => {
    const {data} = await axios.get('/api/certs/' + id, {
        headers: {
            Authorization: token
        }
    })
    dispatch({
        type: 'GET_CERT_EDIT',
        payload: {cert: data, loaded: true, edited: false}
    })
}

export const UPDATE_STATE = () => dispatch => {
    setTimeout(() => {
        dispatch({
            type: 'UPDATE_STATE',
            payload: null
        })
    }, 1000)
}

export const CREATE_NEW_CERT = cert => async dispatch => {
    try {
        dispatch({
            type: 'SAVING_NEW_CERT',
            payload: true
        })
        const {data} = await axios.post('/api/certs/create', cert, {
            headers: {
                Authorization: token
            }
        })
        setTimeout(() => {
            dispatch({
                type: 'UPDATE_STATE',
                payload: null
            })
        }, 5000)
        dispatch({
            type: 'CREATE_NEW_CERT',
            payload: {
                created: true, loading: false, error: false,
                message: {
                    message: '',
                    errors: []
                }
            }
        })
    } catch (e) {
        console.error(e.response)
        if (!e.response.data.hasOwnProperty('errors')) {
            Object.defineProperty(e.response.data, 'errors', {
                value: [],
                enumerable: true
            })
        }
        dispatch({
            type: 'CREATE_NEW_CERT',
            payload: {
                error: true,
                message: e.response.data
            }
        })
    }
}

export const UPDATE_CERT = dataF => async dispatch => {
    try {
        dispatch({
            type: 'SAVING_NEW_CERT',
            payload: true
        })
        const {data} = await axios.patch('/api/certs', dataF, {
            headers: {
                Authorization: token
            }
        })
        setTimeout(() => {
            dispatch({
                type: 'UPDATE_STATE',
                payload: null
            })
        }, 5000)
        dispatch({
            type: 'CREATE_NEW_CERT',
            payload: {
                created: true, loading: false, error: false,
                message: {
                    message: '',
                    errors: []
                }
            }
        })
    } catch (e) {
        console.error(e.response)
        if (!e.response.data.hasOwnProperty('errors')) {
            Object.defineProperty(e.response.data, 'errors', {
                value: [],
                enumerable: true
            })
        }
        dispatch({
            type: 'CREATE_NEW_CERT',
            payload: {
                error: true,
                message: e.response.data
            }
        })
    }
}

export const ASSIGN_ATTENDEES = attendees => async dispatch => {
    try {
        dispatch({
            type: 'ASSIGN_ATTENDEES',
            payload: {
                fetching: true,
                loaded: false,
                not_found: []
            }
        })
        const {data} = await axios.post('/api/certs/assign', attendees, {
            headers: {
                Authorization: token
            }
        })
        dispatch({
            type: 'ASSIGN_ATTENDEES',
            payload: {
                fetching: false,
                loaded: true,
                not_found: data
            }
        })
    } catch (e) {
        dispatch({
            type: 'ASSIGN_ATTENDEES',
            payload: {
                fetching: false,
                loaded: false,
                not_found: []
            }
        })
    }
}

export const DELETE_CERT = id => async dispatch => {
    const {data} = await axios.delete('/api/certs/' + id, {
        headers: {
            Authorization: token
        }
    })
}

export const GET_LAST_ATTENDEES = () => async dispatch => {
    const {data} = await axios.get('/api/attendees/last', {
        headers: {
            'Authorization': token
        }
    })
    dispatch({
        type: 'GET_LAST_ATTENDEES',
        payload: data
    })
}
