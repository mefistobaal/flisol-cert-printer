import React, {useEffect} from 'react';
import {HashRouter as Router, Route, Switch} from "react-router-dom";
import {connect} from "react-redux";
import * as appActions from '../actions/appActions';
import HeaderDesktop from "./components/headerDesktop";
import HeaderMobile from "./components/headerMobile";
import HeaderMobileChild from "./components/headerMobileChild";
import HomeAdmin from "./views/home";
import AdminConfig from "./views/adminConfig";
import Certs from "./views/certs";
import Cursos from "./views/cursos";
import Asistentes from "./views/asistentes";
import CertsCreate from "./views/certsCreate";
import AdminList from "./views/adminList";
import AdminCreate from "./views/adminCreate";
import AdminEdit from "./views/adminEdit";
import Footer from "./components/footer";
import AsistentesCrear from "./views/asistentesCrear";
import CursosCreate from "./views/cursosCreate";
import AsistentesEdit from "./views/asistentesEdit";
import CertsEdit from "./views/certsEdit";
import CursosEdit from "./views/cursosEdit";

const CloseSession = props => {
    useEffect(() => {
        props.closeSession()
    }, [])
    return <></>
}

const AdminRoutes = props => {
    useEffect(() => {
        props.VALIDATE_TOKEN();
        props.GET_USER_DATA();
        props.GET_ACTUAL_CONFIG();
    }, [])
    return (
        <Router>
            <HeaderDesktop img={props.config.admin_logo} userName={props.user.name} userEmail={props.user.email}/>
            <HeaderMobile img={props.config.admin_logo}/>
            <HeaderMobileChild img={props.config.admin_logo} userName={props.user.name} userEmail={props.user.email}/>
            <div className="page-content--bgf7">
                <Switch>
                    <Route path="/" exact>
                        <HomeAdmin userName={props.user.name}/>
                    </Route>
                    <Route path="/home">
                        <HomeAdmin userName={props.user.name}/>
                    </Route>
                    <Route path="/config" component={AdminConfig}/>
                    <Route path="/certificados" component={Certs}/>
                    <Route path="/certificados-crear" component={CertsCreate}/>
                    <Route path="/certificados-editar/:id" component={CertsEdit}/>
                    <Route path="/cursos" component={Cursos}/>
                    <Route path="/curso-crear" component={CursosCreate}/>
                    <Route path="/curso-editar/:id" component={CursosEdit}/>
                    <Route path="/asistentes" component={Asistentes}/>
                    <Route path="/asistentes-crear" component={AsistentesCrear}/>
                    <Route path="/asistentes-editar/:id" component={AsistentesEdit}/>
                    <Route path="/admins" component={AdminList}/>
                    <Route path="/admins-crear" component={AdminCreate}/>
                    <Route path="/admins-editar/:id" component={AdminEdit}/>
                    <Route path="/close"><CloseSession closeSession={props.CLOSE_SESSION}/></Route>
                </Switch>
                <Footer/>
            </div>
        </Router>
    )
}

const mapStateToProps = reducer => {
    return reducer.appReducer
};

export default connect(mapStateToProps, appActions)(AdminRoutes)
