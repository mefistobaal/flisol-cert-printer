import React from 'react'
import {connect} from "react-redux";
import * as attendeesActions from "../../actions/attendeesActions";
import {Alert} from "../components/alert";
import {Link} from "react-router-dom";
import {useForm} from "react-hook-form";

const AsistentesCrear = props => {
    const {register, handleSubmit, errors} = useForm()
    const saveAttendee = data => {
        props.CREATE_ATTENDEE(data)
    }
    console.log(errors)
    return (
        <section className="p-t-20">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-header">
                                <strong>Crear Asistente </strong>
                            </div>
                            <div className="card-body">
                                <form onSubmit={handleSubmit(saveAttendee)}>
                                    <div className="col-10 offset-1">
                                        <div className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="name" className=" form-control-label">Nombres</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <input type="text" id="name" name="first_names" placeholder="Nombres"
                                                       className={`form-control ${errors.first_names ? 'is-invalid' : 'is-valid'}`}
                                                       ref={register({required: true})}/>
                                            </div>
                                        </div>
                                        <div className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="name" className=" form-control-label">Apellidos</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <input type="text" id="name" name="second_names" placeholder="Apellidos"
                                                       className={`form-control ${errors.second_names ? 'is-invalid' : 'is-valid'}`}
                                                       ref={register({required: true})}/>
                                            </div>
                                        </div>
                                        <div className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="select" className=" form-control-label">Tipo de
                                                    documento</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <select name="doc_type" id="select"
                                                        className={`form-control ${errors.doc_type ? 'is-invalid' : 'is-valid'}`}
                                                        ref={register({required: true})}>
                                                    <option value="">Seleccionar</option>
                                                    <option value="CC">Cédula de ciudadanía</option>
                                                    <option value="Passport">Pasaporte</option>
                                                    <option value="TI">Tarjeta de identidad</option>
                                                    <option value="NIT">NIT</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="name" className=" form-control-label">Documento</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <input type="text" id="name" name="document" placeholder="Documento"
                                                       className={`form-control ${errors.document || props.message.errors.hasOwnProperty('document') ? 'is-invalid' : 'is-valid'}`}
                                                       ref={register({required: true})}
                                                />
                                                {props.message.errors.hasOwnProperty('document') ?
                                                    <small>{props.message.errors.document}</small> : null}
                                            </div>
                                        </div>
                                        <div className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="email" className=" form-control-label">Email</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <input type="email" id="email" name="email" placeholder="Email"
                                                       className={`form-control ${errors.email || props.message.errors.hasOwnProperty('email') ? 'is-invalid' : 'is-valid'}`}
                                                       ref={register({required: true})}
                                                />
                                                {props.message.errors.hasOwnProperty('email') ?
                                                    <small>{props.message.errors.email}</small> : null}
                                            </div>
                                        </div>
                                        <div className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="phone" className=" form-control-label">Teléfono</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <input type="text" id="phone" name="phone" placeholder="Teléfono"
                                                       className={`form-control ${errors.phone ? 'is-invalid' : 'is-valid'}`}
                                                       ref={register({required: true})}/>
                                            </div>
                                        </div>
                                        <div className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="status" className=" form-control-label">Estatus</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <label className="switch switch-3d switch-success mr-3">
                                                    <input type="checkbox" className="switch-input"
                                                           name="status"
                                                           id="status"
                                                           ref={register}
                                                           defaultChecked={true}/>
                                                    <span className="switch-label"/>
                                                    <span className="switch-handle"/>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {props.loading
                                        ? <Alert type="warning" title="Cargando..."/>
                                        : <>
                                            <Link to="/asistentes">
                                                <button type="button"
                                                        className="btn btn-outline-secondary m-b-10 mr-5">Cancelar
                                                </button>
                                            </Link>
                                            <button type="submit"
                                                    className="btn btn-outline-success m-b-10">Guardar
                                            </button>
                                        </>
                                    }
                                </form>
                                {props.error ? <Alert type="danger" title={props.message.message}/> : null}
                                {!props.error && props.created ?
                                    <Alert type="success" title={`Se ha creado correctamente`}/> : null}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

const mapStateToProps = reducer => {
    return reducer.attendeeReducer
}

export default connect(mapStateToProps, attendeesActions)(AsistentesCrear)
