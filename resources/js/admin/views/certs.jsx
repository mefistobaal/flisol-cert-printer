import React, {useEffect} from 'react'
import {connect} from 'react-redux'
import * as certsActions from '../../actions/certActions'
import {CertsTable} from "../components/certsTable";

const Certs = props => {
    useEffect(() => {
        props.GET_CERTS()
    }, [])
    return (
        <>
            <CertsTable {...props} />
        </>
    )
}

const mapStateToProps = reducer => {
    return reducer.certReducer
}

export default connect(mapStateToProps, certsActions)(Certs)
