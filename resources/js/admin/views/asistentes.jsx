import React, {useEffect} from 'react'
import {connect} from 'react-redux'
import * as attendeesActions from '../../actions/attendeesActions'
import {AttendeesTable} from "../components/attendeesTable";
import Swal from "sweetalert2";

const Asistentes = props => {
    useEffect(() => {
        props.GET_ATTENDEES()
    }, [])

    useEffect(() => {
        if (props.error) {
            Swal.close()
            Swal.fire({
                title: 'Oops',
                html: props.message.message,
                icon: "error",
                allowOutsideClick: false
            })
        } else if (props.imported) {
            Swal.fire('Correcto!', 'Se ha importado la lista correctamente', 'success').then(() => props.GET_ATTENDEES())
        }
    }, [props.error, props.imported])

    return (
        <AttendeesTable
            data={props.attendees}
            paginator={props.GET_ATTENDEES_PAGE}
            importer={props.IMPORT_ATTENDEES_LIST}
            exporter={props.EXPORT_ATTENDEES_LIST}
            deleter={props.DELETE_ATTENDEE}
            updater={props.GET_ATTENDEES}
            searcher={props.SEARCH_ATTENDEE}
        />
    )
}

const mapStateToProps = reducer => {
    return reducer.attendeeReducer
}

export default connect(mapStateToProps, attendeesActions)(Asistentes)
