import React, {useEffect} from 'react'
import {connect} from 'react-redux'
import AdminsTable from "../components/adminTable"
import * as adminActions from '../../actions/adminActions'

const AdminList = props => {
    useEffect(() => {
        props.GET_ADMINS()
    }, [])

    return (
        <AdminsTable data={props.admins} deleter={props.DELETE_ADMIN} update={props.GET_ADMINS} />
    )
}

const mapStateToProps = reducer => {
    return reducer.adminReducer
}

export default connect(mapStateToProps, adminActions)(AdminList)
