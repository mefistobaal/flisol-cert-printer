import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import * as appActions from '../../actions/appActions';
import {useForm} from "react-hook-form";
import {Alert} from "../components/alert";
import {Link} from "react-router-dom";

const AdminConfig = props => {
    const [loadAgain, setLoadAgain] = useState(false)
    const {register, handleSubmit} = useForm()
    useEffect(() => {
        if (loadAgain) {
            props.GET_ACTUAL_CONFIG()
            setLoadAgain(false)
        }
    }, [loadAgain])

    const submit = async data => {
        await props.SET_ACTUAL_CONFIG(data);
        setLoadAgain(true)
    }
    return (
        <section className="p-t-20">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-header">
                                <strong>Configuraciones del sitio </strong>
                            </div>
                            <div className="card-body card-block">
                                <form onSubmit={handleSubmit(submit)}>
                                    <div className="form-group">
                                        <label htmlFor="site_title" className="form-control-label">Titulo del
                                            sitio</label>
                                        <input type="text" id="site_title" name="site_title"
                                               placeholder="Titulo del sitio"
                                               defaultValue={props.appReducer.config.title_site}
                                               required
                                               className={`form-control ${props.configReducer.message.errors.hasOwnProperty('site_title') ? 'is-invalid' : ''}`}
                                               ref={register}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="favicon" className="form-control-label">Favicon</label>
                                        <input type="file" name="favicon" id="favicon" className="form-control-file"
                                               ref={register}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="admin_logo" className="form-control-label">Logo del panel de
                                            administración</label>
                                        <input type="file" name="admin_logo" id="admin_logo"
                                               className="form-control-file" ref={register}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="home_logo" className="form-control-label">Logo del Home</label>
                                        <input type="file" name="home_logo" id="home_logo" className="form-control-file"
                                               ref={register}/>
                                    </div>
                                    {props.loading
                                        ? <Alert type="warning" title="Cargando..."/>
                                        : <>
                                            <Link to="/certificados">
                                                <button type="button"
                                                        className="btn btn-outline-secondary m-b-10 mr-5">Cancelar
                                                </button>
                                            </Link>
                                            <button type="submit"
                                                    className="btn btn-outline-success m-b-10">Guardar
                                            </button>
                                        </>
                                    }
                                    {props.appReducer.error ?
                                        <Alert type="danger" title={props.appReducer.message.message}/> : null}
                                    {!props.configReducer.error && props.configReducer.saved ?
                                        <Alert type="success" title={`Se ha guardado correctamente`}/> : null}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

const mapStateToProps = reducer => {
    const {configReducer, appReducer} = reducer
    return {configReducer, appReducer}
};

export default connect(mapStateToProps, appActions)(AdminConfig)
