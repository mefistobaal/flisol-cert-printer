import React from 'react'
import {connect} from "react-redux";
import * as coursesActions from "../../actions/coursesActions";
import {Alert} from "../components/alert";
import {Link} from "react-router-dom";
import {useForm} from "react-hook-form";

const CursosCreate = props => {
    const {register, handleSubmit, errors} = useForm()
    const saveCourse = data => {
        props.COURSE_CREATE(data)
    }
    return (
        <section className="p-t-20">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-header">
                                <strong>Crear Curso </strong>
                                {/*<small>{title}</small>*/}
                            </div>
                            <div className="card-body">
                                <form onSubmit={handleSubmit(saveCourse)}>
                                    <div className="col-10 offset-1">
                                        <div className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="name" className=" form-control-label">Nombre</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <input type="name" id="name" name="name" placeholder="Nombre"
                                                       className={`form-control ${errors.name || props.message.errors.hasOwnProperty('name') ? 'is-invalid' : 'is-valid'}`}
                                                       ref={register({required: true})}
                                                />
                                                {props.message.errors.hasOwnProperty('name') ?
                                                    <small>{props.message.errors.name}</small> : null}
                                            </div>
                                        </div>
                                        <div className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="email" className=" form-control-label">Estatus</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <label className="switch switch-3d switch-success mr-3">
                                                    <input type="checkbox" className="switch-input"
                                                           name="status"
                                                           ref={register}
                                                           defaultChecked={true}/>
                                                    <span className="switch-label"/>
                                                    <span className="switch-handle"/>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {props.loading
                                        ? <Alert type="warning" title="Cargando..."/>
                                        : <>
                                            <Link to="/admins">
                                                <button type="button"
                                                        className="btn btn-outline-secondary m-b-10 mr-5">Cancelar
                                                </button>
                                            </Link>
                                            <button type="submit"
                                                    className="btn btn-outline-success m-b-10">Guardar
                                            </button>
                                        </>
                                    }
                                </form>
                                {props.error ? <Alert type="danger" title={props.message.message}/> : null}
                                {!props.error && props.created ?
                                    <Alert type="success" title={`Se ha creado correctamente`}/> : null}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

const mapStateToProps = reducer => {
    return reducer.coursesReducer
}

export default connect(mapStateToProps, coursesActions)(CursosCreate)
