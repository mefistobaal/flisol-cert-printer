import React, {useEffect, useState} from 'react'
import {connect} from 'react-redux'
import * as adminActions from '../../actions/adminActions'
import {Alert} from "../components/alert";
import {Link} from "react-router-dom";
import {useForm} from "react-hook-form";

const AdminCreate = props => {
    const [title, setTitle] = useState()
    const {register, handleSubmit, errors} = useForm()
    const [password, setPassword] = useState()
    const [passConfirm, setPassConfirm] = useState()
    const [validPass, setValidPass] = useState(false)
    useEffect(() => {
        if (password !== passConfirm) {
            setValidPass(false)
        } else {
            setValidPass(true)
        }
    }, [passConfirm, password])
    const saveAdmin = data => {
        props.SAVE_NEW_ADMIN(data)
    }
    return (
        <section className="p-t-20">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-header">
                                <strong>Crear Administrador </strong>
                                <small>{title}</small>
                            </div>
                            <div className="card-body">
                                <form onSubmit={handleSubmit(saveAdmin)}>
                                    <div className="col-10 offset-1">
                                        <div className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="name" className=" form-control-label">Nombre</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <input type="text" id="name" name="name" placeholder="Nombre"
                                                       className={`form-control ${errors.name ? 'is-invalid' : 'is-valid'}`}
                                                       ref={register({required: true})}/>
                                            </div>
                                        </div>
                                        <div className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="email" className=" form-control-label">Email</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <input type="email" id="email" name="email" placeholder="Email"
                                                       className={`form-control ${errors.email || props.message.errors.hasOwnProperty('email') ? 'is-invalid' : 'is-valid'}`}
                                                       ref={register({required: true})}
                                                />
                                                {props.message.errors.hasOwnProperty('email') ?
                                                    <small>{props.message.errors.email}</small> : null}
                                            </div>
                                        </div>
                                        <div className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="password"
                                                       className=" form-control-label">Contraseña</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <input type="password" id="password" name="password"
                                                       placeholder="Contraseña"
                                                       className={`form-control ${validPass || props.message.errors.hasOwnProperty('password') ? 'is-valid' : null}`}
                                                       ref={register({required: true})}
                                                       onChange={event => setPassword(event.target.value)}
                                                />
                                            </div>
                                        </div>
                                        <div className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="password_confirmation" className=" form-control-label">Confirmar
                                                    contraseña</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <input type="password" id="password_confirmation"
                                                       name="password_confirmation"
                                                       placeholder="Confirmar contraseña"
                                                       className={`form-control ${validPass || props.message.errors.hasOwnProperty('password') ? 'is-valid' : 'is-invalid'}`}
                                                       ref={register({required: true})}
                                                       onChange={event => {
                                                           setPassConfirm(event.target.value)
                                                       }}
                                                />
                                                {props.message.errors.hasOwnProperty('password') ?
                                                    <small>{props.message.errors.password}</small> : null}
                                            </div>
                                        </div>
                                        <div className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="email" className=" form-control-label">Estatus</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <label className="switch switch-3d switch-success mr-3">
                                                    <input type="checkbox" className="switch-input"
                                                           name="status"
                                                           ref={register}
                                                           defaultChecked={true}/>
                                                    <span className="switch-label"/>
                                                    <span className="switch-handle"/>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {props.loading
                                        ? <Alert type="warning" title="Cargando..."/>
                                        : <>
                                            <Link to="/admins">
                                                <button type="button"
                                                        className="btn btn-outline-secondary m-b-10 mr-5">Cancelar
                                                </button>
                                            </Link>
                                            <button type="submit"
                                                    className="btn btn-outline-success m-b-10">Guardar
                                            </button>
                                        </>
                                    }
                                </form>
                                {props.error ? <Alert type="danger" title={props.message.message}/> : null}
                                {!props.error && props.created ?
                                    <Alert type="success" title={`Se ha creado correctamente`}/> : null}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

const mapStateToProps = reducer => {
    return reducer.adminReducer
}

export default connect(mapStateToProps, adminActions)(AdminCreate)
