import React, {useEffect, useState} from 'react'
import {connect} from 'react-redux'
import * as certsActions from '../../actions/certActions'
import {Alert} from "../components/alert";
import {Link} from "react-router-dom";
import {bindActionCreators} from "redux";
import * as coursesActions from "../../actions/coursesActions";
import {useForm} from "react-hook-form";

const CertsCreate = props => {
    const [title, setTitle] = useState('')
    const {register, handleSubmit, errors} = useForm()
    const saveCert = data => {
        if (data.background.length > 0) {
            let reader = new FileReader();
            reader.readAsDataURL(data.background[0])
            reader.onload = () => {
                Object.defineProperty(data, 'backgroundData', {
                    enumerable: true,
                    value: reader.result
                })
                if (!props.certReducer.loading) props.certs.CREATE_NEW_CERT(data)
            }
        } else {
            if (!props.certReducer.loading) props.certs.CREATE_NEW_CERT(data)
        }
    }
    useEffect(() => {
        props.courses.GET_COURSES()
    }, [])
    return (
        <section className="p-t-20">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-header">
                                <strong>Certificado </strong>
                                <small>{title}</small>
                            </div>
                            <div className="card-body card-block">
                                <form onSubmit={handleSubmit(saveCert)}>
                                    <div className="row form-group">
                                        <div className="col-6">
                                            <label htmlFor="title" className="form-control-label">Titulo</label>
                                            <input type="text" id="title" name="cert_title"
                                                   placeholder="Ingrese el titulo del certificado"
                                                   className={`form-control ${props.certReducer.message.errors.hasOwnProperty('cert_title') ? 'is-invalid' : ''}`}
                                                   required
                                                   onChange={event => setTitle(event.target.value)}
                                                   ref={register}
                                            />
                                        </div>
                                        <div className="col-6">
                                            <label htmlFor="titleColor" className="form-control-label">Color del
                                                titulo</label>
                                            <input type="color" id="titleColor" name="cert_title_color"
                                                   placeholder="Ingrese el color del titulo del certificado"
                                                   className={`form-control form-control-color ${props.certReducer.message.errors.hasOwnProperty('cert_title_color') ? 'is-invalid' : ''}`}
                                                   required
                                                   onChange={event => setTitle(event.target.value)}
                                                   ref={register}
                                            />
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-6">
                                            <label htmlFor="year" className="form-control-label">Fecha</label>
                                            <input type="date" id="year" name="year" placeholder="Año"
                                                   className={`form-control ${props.certReducer.message.errors.hasOwnProperty('year') ? 'is-invalid' : ''}`}
                                                   required
                                                   ref={register}
                                            />
                                        </div>
                                        <div className="col-6">
                                            <label htmlFor="yearColor" className="form-control-label">Color de la
                                                fecha</label>
                                            <input type="color" id="yearColor" name="year_color"
                                                   placeholder="Ingrese el color del la fecha"
                                                   className={`form-control form-control-color ${props.certReducer.message.errors.hasOwnProperty('year_color') ? 'is-invalid' : ''}`}
                                                   required
                                                   onChange={event => setTitle(event.target.value)}
                                                   ref={register}
                                            />
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-6">
                                            <label htmlFor="celebrated" className="form-control-label">Celebrado
                                                en</label>
                                            <input type="text" id="celebrated" name="celebrated_on"
                                                   placeholder="Celebrado en"
                                                   required
                                                   className={`form-control ${props.certReducer.message.errors.hasOwnProperty('celebrated_on') ? 'is-invalid' : ''}`}
                                                   ref={register}
                                            />
                                        </div>
                                        <div className="col-6">
                                            <label htmlFor="nameColor" className="form-control-label">Color del nombre
                                                del asistente</label>
                                            <input type="color" id="nameColor" name="name_color"
                                                   placeholder="Ingrese el color del la fecha"
                                                   className={`form-control form-control-color ${props.certReducer.message.errors.hasOwnProperty('name_color') ? 'is-invalid' : ''}`}
                                                   required
                                                   onChange={event => setTitle(event.target.value)}
                                                   ref={register}
                                            />
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        {props.coursesReducer.courses.length > 0
                                            ? <div className="col-6">
                                                <label htmlFor="year" className="form-control-label">Curso</label>
                                                <select name="course" ref={register} className="form-control"
                                                        defaultValue={props.certReducer.edit.cert.course?.course_id}>
                                                    <option value="">Seleccionar Curso</option>
                                                    {props.coursesReducer.courses.map(course => (
                                                        <option value={course.id}
                                                                key={course.id}>{course.name}</option>
                                                    ))}
                                                </select>
                                            </div>
                                            : null
                                        }
                                        <div className="col-6">
                                            <label htmlFor="nameColor" className="form-control-label">Fondo</label>
                                            <input type="file" id="background" name="background"
                                                   placeholder="Ingrese el fondo del certificado"
                                                   defaultValue={props.certReducer.edit.cert.background}
                                                   className={`form-control form-control-file ${props.certReducer.message.errors.hasOwnProperty('background') ? 'is-invalid' : ''}`}
                                                   ref={register}
                                            />
                                        </div>
                                    </div>

                                    {props.certReducer.loading
                                        ? <Alert type="warning" title="Cargando..."/>
                                        : <>
                                            <Link to="/certificados">
                                                <button type="button"
                                                        className="btn btn-outline-secondary m-b-10 mr-5">Cancelar
                                                </button>
                                            </Link>
                                            <button type="submit"
                                                    className="btn btn-outline-success m-b-10">Guardar
                                            </button>
                                        </>
                                    }
                                </form>
                                {props.certReducer.error ? <Alert type="danger" title={props.message.message}/> : null}
                                {!props.certReducer.error && props.certReducer.created ?
                                    <Alert type="success" title={`${title} se ha creado correctamente`}/> : null}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        certs: bindActionCreators(certsActions, dispatch),
        courses: bindActionCreators(coursesActions, dispatch)
    }
}

const mapStateToProps = reducer => {
    const {certReducer, coursesReducer} = reducer
    return {certReducer, coursesReducer}
}

export default connect(mapStateToProps, mapDispatchToProps)(CertsCreate)
