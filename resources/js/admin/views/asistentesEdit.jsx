import React, {useEffect} from 'react'
import {connect} from "react-redux";
import * as attendeesActions from "../../actions/attendeesActions";
import * as certActions from "../../actions/certActions"
import {Alert} from "../components/alert";
import {Link} from "react-router-dom";
import {useForm} from "react-hook-form";
import moment from "moment";
import {bindActionCreators} from "redux";
import {AttendeeCertModal} from "../components/attendeesCertModal";

const AsistentesEdit = props => {
    const {register, handleSubmit, errors} = useForm()
    moment.locale('es-mx')
    useEffect(() => {
        props.actions.GET_ATTENDEE_EDIT(props.match.params.id)
    }, [])
    useEffect(() => {
        return function cleanup() {
            props.actions.CLEANUP(props.attendeeReducer.attendees)
        }
    }, [])
    const saveAttendee = data => {
        Object.defineProperty(data, 'id', {
            enumerable: true, value: props.match.params.id
        })
        props.actions.EDIT_ATTENDEE(data)
    }
    const restoreAttendee = id => {
        props.actions.RESTORE_ATTENDEE(id)
    }

    console.log(props)
    return (
        <section className="p-t-20">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-header">
                                <strong>Editar Asistente </strong>
                                <small>{props.attendeeReducer.edit.attendee.first_names + ' ' + props.attendeeReducer.edit.attendee.second_names}</small>
                            </div>
                            <div className="card-body">
                                {!props.attendeeReducer.edit.loaded
                                    ? <Alert type="warning" title="Cargando..."/>
                                    : <form onSubmit={handleSubmit(saveAttendee)}>
                                        <div className="col-10 offset-1">
                                            <div className="row form-group">
                                                <div className="col col-md-3">
                                                    <label htmlFor="names"
                                                           className=" form-control-label">Nombres</label>
                                                </div>
                                                <div className="col-12 col-md-9">
                                                    <input type="text"
                                                           id="names"
                                                           name="first_names"
                                                           placeholder="Nombres"
                                                           defaultValue={props.attendeeReducer.edit.attendee.first_names}
                                                           className={`form-control ${errors.first_names ? 'is-invalid' : 'is-valid'}`}
                                                           ref={register({required: true})}/>
                                                </div>
                                            </div>
                                            <div className="row form-group">
                                                <div className="col col-md-3">
                                                    <label htmlFor="snames"
                                                           className=" form-control-label">Apellidos</label>
                                                </div>
                                                <div className="col-12 col-md-9">
                                                    <input type="text"
                                                           id="snames" name="second_names"
                                                           placeholder="Apellidos"
                                                           defaultValue={props.attendeeReducer.edit.attendee.second_names}
                                                           className={`form-control ${errors.second_names ? 'is-invalid' : 'is-valid'}`}
                                                           ref={register({required: true})}/>
                                                </div>
                                            </div>
                                            <div className="row form-group">
                                                <div className="col col-md-3">
                                                    <label htmlFor="select" className=" form-control-label">Tipo de
                                                        documento</label>
                                                </div>
                                                <div className="col-12 col-md-9">
                                                    <select name="doc_type"
                                                            id="select"
                                                            defaultValue={props.attendeeReducer.edit.attendee.doc_type}
                                                            className={`form-control ${errors.doc_type ? 'is-invalid' : 'is-valid'}`}
                                                            ref={register({required: true})}>
                                                        <option value="">Seleccionar</option>
                                                        <option
                                                            value="CC"
                                                        >
                                                            Cédula de ciudadanía
                                                        </option>
                                                        <option value="Passport"
                                                        >
                                                            Pasaporte
                                                        </option>
                                                        <option value="TI"
                                                        >
                                                            Tarjeta de identidad
                                                        </option>
                                                        <option value="NIT"
                                                        >
                                                            NIT
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="row form-group">
                                                <div className="col col-md-3">
                                                    <label htmlFor="document"
                                                           className=" form-control-label">Documento</label>
                                                </div>
                                                <div className="col-12 col-md-9">
                                                    <input type="text"
                                                           id="document"
                                                           name="document"
                                                           placeholder="Documento"
                                                           defaultValue={props.attendeeReducer.edit.attendee.document}
                                                           className={`form-control ${errors.document || props.attendeeReducer.message.errors.hasOwnProperty('document') ? 'is-invalid' : 'is-valid'}`}
                                                           ref={register({required: true})}
                                                    />
                                                    {props.attendeeReducer.message.errors.hasOwnProperty('document') ?
                                                        <small>{props.message.errors.document}</small> : null}
                                                </div>
                                            </div>
                                            <div className="row form-group">
                                                <div className="col col-md-3">
                                                    <label htmlFor="email" className=" form-control-label">Email</label>
                                                </div>
                                                <div className="col-12 col-md-9">
                                                    <input type="email"
                                                           id="email"
                                                           name="email"
                                                           placeholder="Email"
                                                           defaultValue={props.attendeeReducer.edit.attendee.email}
                                                           className={`form-control ${errors.email || props.attendeeReducer.message.errors.hasOwnProperty('email') ? 'is-invalid' : 'is-valid'}`}
                                                           ref={register({required: true})}
                                                    />
                                                    {props.attendeeReducer.message.errors.hasOwnProperty('email') ?
                                                        <small>{props.attendeeReducer.message.errors.email}</small> : null}
                                                </div>
                                            </div>
                                            <div className="row form-group">
                                                <div className="col col-md-3">
                                                    <label htmlFor="phone"
                                                           className=" form-control-label">Teléfono</label>
                                                </div>
                                                <div className="col-12 col-md-9">
                                                    <input type="text"
                                                           id="phone"
                                                           name="phone"
                                                           placeholder="Teléfono"
                                                           defaultValue={props.attendeeReducer.edit.attendee.phone}
                                                           className={`form-control ${errors.phone ? 'is-invalid' : 'is-valid'}`}
                                                           ref={register({required: true})}/>
                                                </div>
                                            </div>
                                            <div className="row form-group">
                                                <div className="col col-md-3">
                                                    <label htmlFor="status"
                                                           className=" form-control-label">Estatus</label>
                                                </div>
                                                <div className="col-12 col-md-9">
                                                    <label className="switch switch-3d switch-success mr-3">
                                                        <input type="checkbox" className="switch-input"
                                                               name="status"
                                                               id="status"
                                                               ref={register}
                                                               defaultChecked={props.attendeeReducer.edit.attendee.status}/>
                                                        <span className="switch-label"/>
                                                        <span className="switch-handle"/>
                                                    </label>
                                                </div>
                                            </div>
                                            {props.loading
                                                ? <Alert type="warning" title="Cargando..."/>
                                                : <>
                                                    <Link to="/asistentes">
                                                        <button type="button"
                                                                className="btn btn-outline-secondary m-b-10 mr-5">Cancelar
                                                        </button>
                                                    </Link>
                                                    <button type="submit"
                                                            className="btn btn-outline-success m-b-10">Guardar
                                                    </button>
                                                    {props.attendeeReducer.edit.attendee.deleted_at !== null
                                                        ? <button type="button"
                                                                  className="btn btn-outline-warning m-b-10 ml-5"
                                                                  onClick={() => restoreAttendee(props.attendeeReducer.edit.attendee.id)}>
                                                            <i className="fa fa-unlock"/> Restaurar
                                                        </button>
                                                        : null
                                                    }
                                                </>
                                            }
                                        </div>
                                    </form>
                                }
                                {props.attendeeReducer.error ?
                                    <Alert type="danger" title={props.attendeeReducer.message.message}/> : null}
                                {!props.attendeeReducer.error && props.attendeeReducer.created ?
                                    <Alert type="success" title={`Se ha actualizado correctamente`}/> : null}
                                <div className="top-campaign">
                                    <h3 className="title-3 m-b-30">Certificados asignados</h3>
                                    <div className="table-responsive">
                                        <table className="table table-top-campaign">
                                            <tbody>
                                            {
                                                props.attendeeReducer.edit.attendee.certs.length > 0
                                                    ? props.attendeeReducer.edit.attendee.certs.map((cert, idx) => (
                                                        <tr key={idx}>
                                                            <td><a href={`/pdf/${cert.verification_code}`}
                                                                   target="_blank">{cert.cert?.cert_title}</a></td>
                                                            <td>{cert.cert?.celebrated_on.toUpperCase()}</td>
                                                            <td>{cert.cert?.year}</td>
                                                            <td>{cert.cert?.updated_at ? moment(cert.cert?.updated_at).calendar() : null}</td>
                                                        </tr>
                                                    ))
                                                    : null
                                            }
                                            </tbody>
                                        </table>
                                    </div>
                                    <button type="button"
                                            className="btn btn-secondary mb-1"
                                            data-toggle="modal"
                                            data-target="#mediumModal"
                                            onClick={() => props.certActions.GET_CERTS()}
                                    >
                                        Modificar
                                    </button>
                                </div>
                                <AttendeeCertModal {...props}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

const mapStateToProps = reducer => {
    const {attendeeReducer, certReducer} = reducer
    return {attendeeReducer, certReducer}
}

const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators(attendeesActions, dispatch),
        certActions: bindActionCreators(certActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AsistentesEdit)
