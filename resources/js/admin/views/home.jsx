import React, {useEffect} from 'react';
import {connect} from "react-redux";
import * as homeActions from '../../actions/homeActions';
import BreadCrumbs from "../components/breadcrumbs";
import WelcomeMessage from "../components/welcomeMessage";
import Statistics from "../components/statistics";

const HomeAdmin = props => {
    useEffect(() => {
        props.GET_STATS()
    }, [])
    return (
        <>
            <BreadCrumbs crumb="Dashboard"/>
            <WelcomeMessage userName={props.userName}/>
            <Statistics stats={props.stats}/>
            {/*<StatisticsCharts/>*/}
            {/*<DataTables title="Ultimos asistentes registrados" data={props.attendees}/>*/}
        </>
    )
}

const mapStateToProps = reducer => {
    return reducer.homeReducer
};

export default connect(mapStateToProps, homeActions)(HomeAdmin)
