import React, {useEffect} from 'react'
import {connect} from 'react-redux'
import * as coursesActions from '../../actions/coursesActions'
import {Link} from "react-router-dom";
import moment from "moment";

const Cursos = props => {
    useEffect(() => {
        props.GET_COURSES()
    }, [])
    return (
        <section className="p-t-20">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h3 className="title-5 m-b-35">Cursos</h3>
                        <div className="table-data__tool">
                            <div className="table-data__tool-right">
                                <Link to={{
                                    pathname: "/curso-crear"
                                }}>
                                    <button className="au-btn au-btn-icon au-btn--green au-btn--small">
                                        <i className="zmdi zmdi-plus"/>Agregar
                                    </button>
                                </Link>
                            </div>
                        </div>
                        <div className="table-responsive table-responsive-data2">
                            <table className="table table-data2">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Estado</th>
                                    <th>Creado en</th>
                                    <th>Certificados</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {props.courses.map(curso => (
                                    <tr className="tr-shadow" key={curso.id}>
                                        <td>{curso.name}</td>
                                        <td>{curso.status
                                            ? <span className="badge badge-success">Activo</span>
                                            : <span className="badge badge-dark">Inactivo</span>}</td>
                                        <td>{moment(curso.created_at).calendar()}</td>
                                        <td>{curso.certs.length}</td>
                                        <td>
                                            <div className="table-data-feature">
                                                <Link to={{
                                                    pathname: '/curso-editar/' + curso.id
                                                }}>
                                                    <button className="item" data-toggle="tooltip" data-placement="top"
                                                            title="Editar">
                                                        <i className="zmdi zmdi-edit"/>
                                                    </button>
                                                </Link>
                                                <button className="item" data-toggle="tooltip" data-placement="top"
                                                        title="Delete">
                                                    <i className="zmdi zmdi-delete"/>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

const mapStateToProps = reducer => {
    return reducer.coursesReducer
}

export default connect(mapStateToProps, coursesActions)(Cursos)
