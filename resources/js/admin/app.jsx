import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, compose, createStore} from 'redux';
import {Provider} from "react-redux";
import reduxThunk from 'redux-thunk';
import reducers from '../reducers/admin/index';
import AdminRoutes from './routes';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
const store = createStore(
    reducers,
    /* preloadedState, */
    compose(applyMiddleware(reduxThunk), composeEnhancers)
);

export default class AdminApp extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <AdminRoutes/>
            </Provider>
        );
    }
}
if (document.getElementById('adminApp')) {
    ReactDOM.render(<AdminApp/>, document.getElementById('adminApp'))
}
