import React, {useEffect, useState} from 'react';
import moment from 'moment'
import {Link} from "react-router-dom";
import {CertAttendeeAssignModal} from "./certAttendeeAsignModal";

export const CertsTable = props => {
    moment.locale('es-mx')
    const [certId, setCertId] = useState()
    const [goToPage, setGoToPage] = useState(null)
    const [search, setSearch] = useState()

    useEffect(() => {
        if (goToPage !== null) {
            props.GET_CERTS_PAGE(goToPage)
        }
    }, [goToPage])

    const deleteCert = (id) => {
        props.DELETE_CERT(id)
        props.GET_CERTS()
    }

    const searchCert = () => {
        props.SEARCH_CERT(search)
    }

    return (
        <section className="p-t-20">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h3 className="title-5 m-b-35">Certificados</h3>
                        <div className="table-data__tool">
                            <div className="table-data__tool-right">
                                <Link to={{
                                    pathname: "/certificados-crear"
                                }}>
                                    <button className="au-btn au-btn-icon au-btn--green au-btn--small">
                                        <i className="zmdi zmdi-plus"/>Agregar
                                    </button>
                                </Link>
                            </div>
                            <div className="table-data__tool-right">
                                <div className="input-group">
                                    <div className="input-group-btn">
                                        <button className="btn btn-primary" onClick={() => searchCert()}>
                                            <i className="fa fa-search"/> Buscar
                                        </button>
                                    </div>

                                    <input type="text"
                                           name="search"
                                           placeholder="Certificado"
                                           className="form-control"
                                           onChange={event => setSearch(event.target.value)}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="table-responsive table-responsive-data2">
                            <table className="table table-data2">
                                <thead>
                                <tr>
                                    <th>Titulo</th>
                                    <th>Año</th>
                                    <th>Celebrado en</th>
                                    <th>Creado en</th>
                                    <th>Asistentes</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {props.certs.data.map(cert => (
                                    <tr className="tr-shadow" key={cert.id}>
                                        <td>{cert.cert_title}</td>
                                        <td>
                                            <span className="block-email">{cert.year}</span>
                                        </td>
                                        <td>{cert.celebrated_on.toUpperCase()}</td>
                                        <td>{moment(cert.created_at).calendar()}</td>
                                        <td>{cert.attendees_count}</td>
                                        <td>
                                            <div className="table-data-feature">
                                                <Link to={{
                                                    pathname: '/certificados-editar/' + cert.id
                                                }}>
                                                    <button className="item" data-toggle="tooltip"
                                                            data-placement="top"
                                                            title="Edit">
                                                        <i className="zmdi zmdi-edit"/>
                                                    </button>
                                                </Link>
                                                <button className="item"
                                                        data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="Delete"
                                                        onClick={() => deleteCert(cert.id)}
                                                >
                                                    <i className="zmdi zmdi-delete"/>
                                                </button>
                                                <button className="item"
                                                    // data-toggle="tooltip"
                                                        data-placement="top"
                                                        data-toggle="modal"
                                                        data-target="#mediumModal"
                                                        data-original-title="More"
                                                        onClick={() => setCertId(cert.id)}
                                                >
                                                    <i className="zmdi zmdi-more"/>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                            {props.certs.total > 15
                                ? <nav aria-label="Page navigation example">
                                    <ul className="pagination justify-content-center">
                                        {props.certs.links.map((link, index) => (
                                            <li className={
                                                `page-item ${link.active
                                                    ? 'active'
                                                    : props.certs.current_page === 1 && link.url === null
                                                        ? 'disabled'
                                                        : link.url === null
                                                            ? 'disabled'
                                                            : null
                                                }`
                                            }
                                                key={index}>
                                                <button onClick={() => setGoToPage(link.url)}
                                                        className="page-link">{link.label}
                                                </button>
                                            </li>
                                        ))}
                                    </ul>
                                </nav>
                                : null
                            }
                            <CertAttendeeAssignModal cert={certId} {...props} />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
