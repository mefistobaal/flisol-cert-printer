import React from 'react';
import {Link} from 'react-router-dom';

export default function HeaderDesktop(props) {
    return (
        <header className="header-desktop3 d-none d-lg-block">
            <div className="section__content section__content--p35">
                <div className="header3-wrap">
                    <div className="header__logo">
                        <Link to={{
                            pathname: '/home'
                        }}>
                            <img src={props.img} alt=""/>
                        </Link>
                    </div>
                    <div className="header__navbar">
                        <ul className="list-unstyled">
                            <li className="has-sub">
                                <a href="#">
                                    <i className="fas fa-tachometer-alt"></i>Dashboard
                                    <span className="bot-line"></span>
                                </a>
                                <ul className="header3-sub-list list-unstyled">
                                    <li>
                                        <Link to={{
                                            pathname: '/config'
                                        }}>
                                            Configuración del sitio
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={{
                                            pathname: '/admins'
                                        }}>
                                            Administradores
                                        </Link>
                                    </li>
                                    {/*<li>*/}
                                    {/*    <a href="index3.html">Historiales</a>*/}
                                    {/*</li>*/}
                                </ul>
                            </li>
                            <li className="has-sub">
                                <Link to={{pathname: '/cursos'}}>
                                    <i className="fas fa-book"></i>Cursos
                                    <span className="bot-line"></span>
                                </Link>
                            </li>
                            <li>
                                <Link to={{pathname: '/certificados'}}>
                                    <i className="fa fa-file-text"/>
                                    <span className="bot-line"/>Certificados
                                </Link>
                            </li>
                            <li>
                                <Link to="/asistentes">
                                    <i className="fa fa-check"/>
                                    <span className="bot-line"/>Asistentes
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div className="header__tool">
                        {/*<div className="header-button-item has-noti js-item-menu">*/}
                        {/*    <i className="zmdi zmdi-notifications"></i>*/}
                        {/*    <div className="notifi-dropdown notifi-dropdown--no-bor js-dropdown">*/}
                        {/*        <div className="notifi__title">*/}
                        {/*            <p>You have 3 Notifications</p>*/}
                        {/*        </div>*/}
                        {/*        <div className="notifi__item">*/}
                        {/*            <div className="bg-c1 img-cir img-40">*/}
                        {/*                <i className="zmdi zmdi-email-open"></i>*/}
                        {/*            </div>*/}
                        {/*            <div className="content">*/}
                        {/*                <p>You got a email notification</p>*/}
                        {/*                <span className="date">April 12, 2018 06:50</span>*/}
                        {/*            </div>*/}
                        {/*        </div>*/}
                        {/*        <div className="notifi__item">*/}
                        {/*            <div className="bg-c2 img-cir img-40">*/}
                        {/*                <i className="zmdi zmdi-account-box"></i>*/}
                        {/*            </div>*/}
                        {/*            <div className="content">*/}
                        {/*                <p>Your account has been blocked</p>*/}
                        {/*                <span className="date">April 12, 2018 06:50</span>*/}
                        {/*            </div>*/}
                        {/*        </div>*/}
                        {/*        <div className="notifi__item">*/}
                        {/*            <div className="bg-c3 img-cir img-40">*/}
                        {/*                <i className="zmdi zmdi-file-text"></i>*/}
                        {/*            </div>*/}
                        {/*            <div className="content">*/}
                        {/*                <p>You got a new file</p>*/}
                        {/*                <span className="date">April 12, 2018 06:50</span>*/}
                        {/*            </div>*/}
                        {/*        </div>*/}
                        {/*        <div className="notifi__footer">*/}
                        {/*            <a href="#">All notifications</a>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        {/*<div className="header-button-item js-item-menu">*/}
                        {/*    <i className="zmdi zmdi-settings"></i>*/}
                        {/*    <div className="setting-dropdown js-dropdown">*/}
                        {/*        <div className="account-dropdown__body">*/}
                        {/*            <div className="account-dropdown__item">*/}
                        {/*                <a href="#">*/}
                        {/*                    <i className="zmdi zmdi-account"></i>Account</a>*/}
                        {/*            </div>*/}
                        {/*            <div className="account-dropdown__item">*/}
                        {/*                <a href="#">*/}
                        {/*                    <i className="zmdi zmdi-settings"></i>Setting</a>*/}
                        {/*            </div>*/}
                        {/*            <div className="account-dropdown__item">*/}
                        {/*                <a href="#">*/}
                        {/*                    <i className="zmdi zmdi-money-box"></i>Billing</a>*/}
                        {/*            </div>*/}
                        {/*        </div>*/}
                        {/*        <div className="account-dropdown__body">*/}
                        {/*            <div className="account-dropdown__item">*/}
                        {/*                <a href="#">*/}
                        {/*                    <i className="zmdi zmdi-globe"></i>Language</a>*/}
                        {/*            </div>*/}
                        {/*            <div className="account-dropdown__item">*/}
                        {/*                <a href="#">*/}
                        {/*                    <i className="zmdi zmdi-pin"></i>Location</a>*/}
                        {/*            </div>*/}
                        {/*            <div className="account-dropdown__item">*/}
                        {/*                <a href="#">*/}
                        {/*                    <i className="zmdi zmdi-email"></i>Email</a>*/}
                        {/*            </div>*/}
                        {/*            <div className="account-dropdown__item">*/}
                        {/*                <a href="#">*/}
                        {/*                    <i className="zmdi zmdi-notifications"></i>Notifications</a>*/}
                        {/*            </div>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        <div className="account-wrap">
                            <div className="account-item account-item--style2 clearfix js-item-menu">
                                <div className="image">
                                    <img src="images/icon/avatar-01.jpg" alt="Admin"/>
                                </div>
                                <div className="content">
                                    <a className="js-acc-btn" href="#">{props.userName}</a>
                                </div>
                                <div className="account-dropdown js-dropdown">
                                    <div className="info clearfix">
                                        <div className="image">
                                            <a href="#">
                                                <img src="images/icon/avatar-01.jpg" alt="John Doe"/>
                                            </a>
                                        </div>
                                        <div className="content">
                                            <h5 className="name">
                                                <a href="#">{props.userName}</a>
                                            </h5>
                                            <span className="email">{props.userEmail}</span>
                                        </div>
                                    </div>
                                    {/*<div className="account-dropdown__body">*/}
                                    {/*    <div className="account-dropdown__item">*/}
                                    {/*        <a href="#">*/}
                                    {/*            <i className="zmdi zmdi-account"></i>Account</a>*/}
                                    {/*    </div>*/}
                                    {/*    <div className="account-dropdown__item">*/}
                                    {/*        <a href="#">*/}
                                    {/*            <i className="zmdi zmdi-settings"></i>Setting</a>*/}
                                    {/*    </div>*/}
                                    {/*    <div className="account-dropdown__item">*/}
                                    {/*        <a href="#">*/}
                                    {/*            <i className="zmdi zmdi-money-box"></i>Billing</a>*/}
                                    {/*    </div>*/}
                                    {/*</div>*/}
                                    <div className="account-dropdown__footer">
                                        <Link to={{pathname: "/close"}}>
                                            <i className="zmdi zmdi-power"></i>Logout
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}
