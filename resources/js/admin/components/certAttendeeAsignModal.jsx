import React from 'react'
import {useForm} from "react-hook-form";

export const CertAttendeeAssignModal = props => {
    const {register, handleSubmit, errors} = useForm()
    const found = cert_id => {
        // return !!props.attendeeReducer.edit.attendee.certs.find(element => element.cert_id === cert_id)
    }
    const editCerts = data => {
        Object.defineProperty(data, 'cert_id', {
            enumerable: true,
            value: props.cert
        })
        props.ASSIGN_ATTENDEES(data)
        props.GET_CERTS()
        // props.actions.ATTENDEE_CERT_ASSIGN(data)
        // props.actions.GET_ATTENDEE_EDIT(props.attendeeReducer.edit.attendee.id)
        // console.log(data)
    }
    return (
        <div className="modal fade" id="mediumModal" tabIndex="-1" role="dialog"
             aria-labelledby="mediumModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-lg" role="document">
                <div className="modal-content">
                    <form onSubmit={handleSubmit(editCerts)}>
                        <div className="modal-header">
                            <h5 className="modal-title" id="mediumModalLabel">Asignar asistentes</h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="form-check">
                                <p>Ingrese la lista de los documentos de los asistentes registrados separados por comas
                                    (,)</p>
                                <textarea name="attendeesList" cols="30" rows="15" className="form-control"
                                          ref={register} disabled={props.assigned.fetching}/>
                            </div>
                        </div>
                        {props.assigned.loaded
                            ? <div className="alert alert-success" role="alert">
                                <h4 className="alert-heading">Asistentes Agregados!</h4>
                                <p>A continuación te mostramos una lista de documentos que posiblemente no pudieron ser
                                    agregados:</p>
                                <hr/>
                                <ul className="ml-2">
                                    {props.assigned.not_found.map(missed => (
                                        <li key={missed}>{missed}</li>
                                    ))}
                                </ul>
                            </div>
                            : null
                        }
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary"
                                    data-dismiss="modal">Cancelar
                            </button>
                            <button type="submit" className="btn btn-primary">Confirmar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
