import React from 'react';

export default class Footer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <section className="p-t-60 p-b-20">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="copyright">
                                {/*<p>Desarrollado por <a href="https://gitlab.com/mefistobaal" target="_blank">Santiago Hurtado</a>.</p>*/}
                                <p>Desarrollado para RadioFest - MinTIC.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
