import React from 'react';
import moment from 'moment'
import {Link} from "react-router-dom";

export default class AdminsTable extends React.Component {
    constructor(props) {
        super(props);
        this.deleteAdmin = this.deleteAdmin.bind(this)
        moment.locale('es-mx')
    }

    deleteAdmin(id) {
        this.props.deleter(id)
        this.props.update()
    }

    render() {
        return (
            <section className="p-t-20">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <h3 className="title-5 m-b-35">Administradores</h3>
                            <div className="table-data__tool">
                                <div className="table-data__tool-right">
                                    <Link to={{
                                        pathname: "/admins-crear"
                                    }}>
                                        <button className="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i className="zmdi zmdi-plus"/>Agregar
                                        </button>
                                    </Link>
                                </div>
                            </div>
                            <div className="table-responsive table-responsive-data2">
                                <table className="table table-data2">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Email</th>
                                        <th>Estado</th>
                                        <th>Creado en</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.props.data.map(admin => (
                                        <tr className="tr-shadow" key={admin.id}>
                                            <td>{admin.name}</td>
                                            <td>
                                                <span className="block-email">{admin.email}</span>
                                            </td>
                                            <td>{admin.status
                                                ? <span className="badge badge-success">Activo</span>
                                                : <span className="badge badge-dark">Inactivo</span>}</td>
                                            <td>{moment(admin.created_at).calendar()}</td>
                                            <td>
                                                <div className="table-data-feature">
                                                    <Link to={{
                                                        pathname: '/admins-editar/' + admin.id
                                                    }}>
                                                        <button className="item" data-toggle="tooltip"
                                                                data-placement="top"
                                                                title="Edit"
                                                        >
                                                            <i className="zmdi zmdi-edit"/>
                                                        </button>
                                                    </Link>

                                                    <button className="item" data-toggle="tooltip" data-placement="top"
                                                            title="Delete" onClick={() => this.deleteAdmin(admin.id)}>
                                                        <i className="zmdi zmdi-delete"/>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
