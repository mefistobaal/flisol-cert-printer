import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import Swal from "sweetalert2";

export const AttendeesTable = props => {
    const [goToPage, setGoToPage] = useState(null)
    const [document, setDocument] = useState()
    const [docType, setDocType] = useState()

    useEffect(() => {
        if (goToPage !== null) {
            props.paginator(goToPage)
        }
    }, [goToPage])

    const importAttendees = async () => {
        const {value: file} = await Swal.fire({
            title: 'Seleccionar csv o excel',
            input: 'file',
            inputAttributes: {
                'aria-label': 'Subir la lista de asistentes'
            }
        })
        if (file) {
            Swal.fire({
                onOpen(popup) {
                    Swal.showLoading()
                },
                allowOutsideClick: () => !Swal.isLoading()
            })
            props.importer(file)
        }
    }
    const exportAttendees = async () => {
        props.exporter()
    }

    const attendeeDelete = id => {
        props.deleter(id)
        props.updater()
    }

    const searchAttendee = () => {
        props.searcher(docType, document)
    }

    return (
        <section className="p-t-20">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <h3 className="title-5 m-b-35">Asistentes</h3>
                        <div className="table-data__tool">
                            <div className="table-data__tool-right">
                                <Link to={{
                                    pathname: "/asistentes-crear"
                                }}>
                                    <button className="au-btn au-btn-icon au-btn--green au-btn--small">
                                        <i className="zmdi zmdi-plus"/>Agregar
                                    </button>
                                </Link>
                                <button className="au-btn au-btn-icon au-btn--blue au-btn--small ml-1"
                                        onClick={importAttendees}>
                                    <i className="zmdi zmdi-cloud-upload"/>Importar
                                </button>
                                <button className="au-btn au-btn-icon au-btn--blue2 au-btn--small ml-1"
                                        onClick={exportAttendees}>
                                    <i className="zmdi zmdi-download"/>Exportar
                                </button>
                            </div>
                            <div className="table-data__tool-right">
                                <div className="input-group">
                                    <div className="input-group-btn">
                                        <button className="btn btn-primary" onClick={() => searchAttendee()}>
                                            <i className="fa fa-search"/> Buscar
                                        </button>
                                    </div>
                                    <div className="rs-select2--light rs-select2--md">
                                        <select className="form-control"
                                                name="property"
                                                defaultValue={"selected"}
                                                onChange={event => setDocType(event.target.value)}
                                        >
                                            <option value="selected">Todo</option>
                                            <option value="CC">Cédula de ciudadanía</option>
                                            <option value="TI">Tarjeta de identidad</option>
                                            <option value="NIT">NIT</option>
                                            <option value="Passport">Pasaporte</option>
                                        </select>
                                        <div className="dropDownSelect2"/>
                                    </div>
                                    <input type="text"
                                           name="search"
                                           placeholder="Documento"
                                           className="form-control"
                                           onChange={event => setDocument(event.target.value)}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="table-responsive table-responsive-data2">
                            <table className="table table-data2">
                                <thead>
                                <tr>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Documento</th>
                                    <th>Email</th>
                                    <th>Celular</th>
                                    <th>Estado</th>
                                    <th>Certificados</th>
                                    <th/>
                                </tr>
                                </thead>
                                <tbody>
                                {props.data.data.map(attendee => (
                                    <tr className="tr-shadow" key={attendee.id}>
                                        <td>{attendee.first_names}</td>
                                        <td>{attendee.second_names}</td>
                                        <td>{`${attendee.doc_type} ${attendee.document}`}</td>
                                        <td><span className="block-email">{attendee.email}</span></td>
                                        <td>{attendee.phone}</td>
                                        <td>{attendee.status
                                            ? <span className="badge badge-success">Activo</span>
                                            : <span className="badge badge-dark">Inactivo</span>}
                                        </td>
                                        <td>{attendee.certs.length}</td>
                                        <td>
                                            <div className="table-data-feature">
                                                <Link to={{
                                                    pathname: `/asistentes-editar/${attendee.id}`,
                                                }}>
                                                    <button className="item" data-toggle="tooltip" data-placement="top"
                                                            title="Editar">
                                                        <i className="zmdi zmdi-edit"/>
                                                    </button>
                                                </Link>
                                                <button className="item"
                                                        data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="Eliminar"
                                                        onClick={() => attendeeDelete(attendee.id)}
                                                >
                                                    <i className="zmdi zmdi-delete"/>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                            {props.data.total > 15
                                ? <nav aria-label="Page navigation example">
                                    <ul className="pagination justify-content-center">
                                        {props.data.links.map((link, index) => (
                                            <li className={
                                                `page-item ${link.active
                                                    ? 'active'
                                                    : props.data.current_page === 1 && link.url === null
                                                        ? 'disabled'
                                                        : link.url === null
                                                            ? 'disabled'
                                                            : null
                                                }`
                                            }
                                                key={index}>
                                                <button onClick={() => setGoToPage(link.url)}
                                                        className="page-link">{link.label}
                                                </button>
                                            </li>
                                        ))}
                                    </ul>
                                </nav>
                                : null
                            }
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
