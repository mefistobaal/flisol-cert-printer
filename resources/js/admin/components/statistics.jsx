import React from 'react';

export default function Statistics(props) {
    return (
        <section className="statistic statistic2">
            <div className="container">
                <div className="row">
                    <div className="col-md-6 col-lg-3">
                        <div className="statistic__item statistic__item--green">
                            <h2 className="number">{props.stats.users}</h2>
                            <span className="desc">Participantes registrados</span>
                            <div className="icon">
                                <i className="zmdi zmdi-account-o"/>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-lg-3">
                        <div className="statistic__item statistic__item--orange">
                            <h2 className="number">{props.stats.certs.certs_gen}</h2>
                            <span className="desc">Certificados generados</span>
                            <div className="icon">
                                <i className="zmdi zmdi-shopping-cart"/>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-lg-3">
                        <div className="statistic__item statistic__item--blue">
                            <h2 className="number">{props.stats.certs.certs_sended}</h2>
                            <span className="desc">Certificados enviados</span>
                            <div className="icon">
                                <i className="zmdi zmdi-calendar-note"/>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-lg-3">
                        <div className="statistic__item statistic__item--red">
                            <h2 className="number">{props.stats.certs.querys_gen}</h2>
                            <span className="desc">Consultas generadas</span>
                            <div className="icon">
                                <i className="zmdi zmdi-calendar-note"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
