import React from 'react'

export const Alert = props => {

    return (
        <div className={`sufee-alert alert with-close alert-${props.type} alert-dismissible fade show`}>
            <span className={`badge badge-pill badge-${props.type}`}>{props.title}</span>
            {props.text}
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    )
}
