import React from 'react';
import {Link} from "react-router-dom";

export default function HeaderMobile(props) {
    console.log(props)
    return (
        <header className="header-mobile header-mobile-2 d-block d-lg-none">
            <div className="header-mobile__bar">
                <div className="container-fluid">
                    <div className="header-mobile-inner">
                        <a className="logo" href="#">
                            <img src={props.img} alt=""/>
                        </a>
                        <button className="hamburger hamburger--slider" type="button">
                            <span className="hamburger-box">
                                <span className="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav className="navbar-mobile">
                <div className="container-fluid">
                    <ul className="navbar-mobile__list list-unstyled">
                        <li className="has-sub">
                            <a className="js-arrow" href="#">
                                <i className="fas fa-tachometer-alt"></i>Dashboard
                            </a>
                            <ul className="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <Link to={{
                                        pathname: '/config'
                                    }}>
                                        Configuración del sitio
                                    </Link>
                                </li>
                                <li>
                                    <Link to={{
                                        pathname: '/admins'
                                    }}>
                                        Administradores
                                    </Link>
                                </li>
                                {/*<li>*/}
                                {/*    <a href="index3.html">Historiales</a>*/}
                                {/*</li>*/}
                            </ul>
                        </li>
                        <li className="has-sub">
                            <Link to={{pathname: '/cursos'}}>
                                <i className="fas fa-book"/>Cursos
                                <span className="bot-line"/>
                            </Link>
                        </li>
                        <li>
                            <Link to={{pathname: '/certificados'}}>
                                <i className="fa fa-file-text"/>
                                <span className="bot-line"/>Certificados
                            </Link>
                        </li>
                        <li>
                            <Link to="/asistentes">
                                <i className="fa fa-check"/>
                                <span className="bot-line"/>Asistentes
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    )
}
