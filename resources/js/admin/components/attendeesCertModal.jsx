import React from 'react'
import {useForm} from "react-hook-form";

export const AttendeeCertModal = props => {
    const {register, handleSubmit, errors} = useForm()
    const found = cert_id => {
        return !!props.attendeeReducer.edit.attendee.certs.find(element => element.cert_id === cert_id)
    }
    const editCerts = data => {
        Object.defineProperty(data, 'attendee_id', {
            enumerable: true,
            value: props.attendeeReducer.edit.attendee.id
        })
        props.actions.ATTENDEE_CERT_ASSIGN(data)
        props.actions.GET_ATTENDEE_EDIT(props.attendeeReducer.edit.attendee.id)
        // console.log(data)
    }
    return (
        <div className="modal fade" id="mediumModal" tabIndex="-1" role="dialog"
             aria-labelledby="mediumModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-lg" role="document">
                <div className="modal-content">
                    <form onSubmit={handleSubmit(editCerts)}>
                        <div className="modal-header">
                            <h5 className="modal-title" id="mediumModalLabel">Certificados</h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">

                            <div className="form-check">
                                {props.certReducer.certs.data.map(cert => (
                                    <div className="checkbox" key={cert.id}>
                                        <label htmlFor={cert.cert_title}
                                               className="form-check-label ">
                                            <input type="checkbox"
                                                   id={cert.cert_title}
                                                   name="certs"
                                                   className="form-check-input"
                                                   value={cert.id}
                                                   defaultChecked={found(cert.id)}
                                                   ref={register}
                                            />
                                            {cert.cert_title}
                                        </label>
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary"
                                    data-dismiss="modal">Cancelar
                            </button>
                            <button type="submit" className="btn btn-primary">Confirmar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
