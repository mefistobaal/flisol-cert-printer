import React from 'react';

export default function WelcomeMessage(props) {
    return (
        <section className="welcome p-t-10">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h1 className="title-4">Bienvenido
                            <span> {props.userName}</span>
                        </h1>
                        <hr className="line-seprate"/>
                    </div>
                </div>
            </div>
        </section>
    )
}
