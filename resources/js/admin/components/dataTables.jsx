import React from 'react';

export default class DataTables extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        console.log(this.props.data)
        return (
            <section className="p-t-20">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <h3 className="title-5 m-b-35">{this.props.title}</h3>
                            <div className="table-data__tool">
                                <div className="table-data__tool-right">
                                    <button className="au-btn au-btn-icon au-btn--green au-btn--small">
                                        <i className="zmdi zmdi-plus"/>Agregar
                                    </button>
                                </div>
                            </div>
                            <div className="table-responsive table-responsive-data2">
                                <table className="table table-data2">
                                    <thead>
                                    <tr>
                                        <th>Nombres</th>
                                        <th>email</th>
                                        <th>telefono</th>
                                        <th>documento</th>
                                        <th>creado</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.props.data.map(attendee => (
                                        <tr className="tr-shadow">
                                            <td>{attendee.first_names + ' ' + attendee.second_names}</td>
                                            <td>
                                                <span className="block-email">{attendee.email}</span>
                                            </td>
                                            <td>{attendee.phone}</td>
                                            <td>{attendee.doc_type + ' ' + attendee.document}</td>
                                            <td>{attendee.created_at}</td>
                                            <td>
                                                <div className="table-data-feature">
                                                    <button className="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                        <i className="zmdi zmdi-edit"/>
                                                    </button>
                                                    <button className="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i className="zmdi zmdi-delete"/>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
