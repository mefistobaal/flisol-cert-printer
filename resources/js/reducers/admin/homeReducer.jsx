const INITIAL_STATE = {
    stats: {
        certs: {
            certs_gen: 0,
            certs_sended: 0,
            querys_gen: 0
        },
        users: 0
    },
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'GET_STATS':
            return {...state, stats: action.payload};
        default:
            return state;
    }
}
