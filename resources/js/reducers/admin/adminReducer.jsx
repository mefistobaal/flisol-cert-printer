const INITIAL_STATE = {
    admins: [],
    created: null,
    loading: false,
    error: false,
    message: {
        message: '',
        errors: []
    },
    edit: {
        name: '',
        email: '',
        status: true
    }
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'GET_ADMINS':
            return {...state, admins: action.payload}
        case 'SAVE_NEW_ADMIN':
            return {
                ...state,
                created: action.payload.created,
                loading: action.payload.loading,
                error: action.payload.error,
                message: action.payload.message
            }
        case 'GET_ADMIN_EDIT':
            return {...state, edit: action.payload}
        default:
            return state
    }
}
