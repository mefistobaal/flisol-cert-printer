const INITIAL_STATE = {
    user: [],
    config: {
        admin_logo: "images/icon/logo-white.png"
    },
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'GET_USER_DATA':
            return {...state, user: action.payload};
        case 'GET_ACTUAL_CONFIG':
            return {...state, config: action.payload};
        default:
            return state
    }
}
