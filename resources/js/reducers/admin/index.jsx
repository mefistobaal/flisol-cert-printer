import {combineReducers} from "redux";
import appReducer from "./appReducer";
import homeReducer from "./homeReducer";
import configReducer from './configReducer';
import certReducer from "./certReducer";
import adminReducer from './adminReducer'
import attendeeReducer from "./attendeeReducer";
import coursesReducer from "./coursesReducer"

export default combineReducers({
    appReducer,
    homeReducer,
    configReducer,
    certReducer,
    adminReducer,
    attendeeReducer,
    coursesReducer
});
