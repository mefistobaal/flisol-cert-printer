const INITIAL_STATE = {
    config: [],
    error: false,
    saved: false,
    message: {
        errors: {}
    },
    loading: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'GET_ACTUAL_CONFIG':
            return {...state, config: action.payload};
        case 'SAVE_ACTUAL_CONFIG':
            return {...state, saved: action.payload.saved, error: action.payload.error, message: action.payload.message}
        default:
            return state;
    }
}
