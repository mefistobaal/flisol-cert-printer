const INITIAL_STATE = {
    certs: {
        total: 0,
        data: []
    },
    created: null,
    loading: false,
    error: false,
    message: {
        message: '',
        errors: []
    },
    edit: {
        cert: {},
        edited: false,
        loaded: false
    },
    assigned: {
        fetching: false,
        not_found: []
    }
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'GET_CERTS':
            return {...state, certs: action.payload}
        case 'GET_CERT_EDIT':
            return {...state, edit: action.payload}
        case 'SAVING_NEW_CERT':
            return {...state, loading: action.payload}
        case 'CREATE_NEW_CERT':
            return {
                ...state,
                created: action.payload.created,
                loading: false,
                error: action.payload.error,
                message: action.payload.message
            }
        case 'ASSIGN_ATTENDEES':
            return {...state, assigned: action.payload}
        case 'UPDATE_STATE':
            return {
                ...state, created: action.payload, edit: {
                    cert: {},
                    edited: false,
                    loaded: false
                }
            }
        default:
            return state
    }
}
