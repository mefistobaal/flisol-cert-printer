const INITIAL_STATE = {
    attendees: {
        data: []
    },
    error: false,
    message: {message: '', errors: []},
    imported: false,
    loading: null,
    created: false,
    edit: {
        loaded: false,
        attendee: {
            certs: []
        }
    }
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'GET_ATTENDEES':
            return {
                ...state,
                attendees: action.payload.attendees,
                message: action.payload.message,
                error: action.payload.error
            }
        case 'IMPORT_ATTENDEES_LIST':
            return {
                ...state,
                imported: action.payload.imported,
                error: action.payload.error,
                message: action.payload.message,
                loading: action.payload.loading
            }
        case 'CREATE_ATTENDEE':
            return {
                ...state,
                created: action.payload.created,
                error: action.payload.error,
                message: action.payload.message,
                loading: action.payload.loading
            }
        case 'EDIT_ATTENDEE':
            return {
                ...state,
                created: action.payload.created,
                error: action.payload.error,
                message: action.payload.message,
                loading: action.payload.loading
            }
        case 'GET_ATTENDEE_EDIT':
            return {...state, edit: action.payload}
        case 'CLEANUP':
            return action.payload
        default:
            return state
    }
}
