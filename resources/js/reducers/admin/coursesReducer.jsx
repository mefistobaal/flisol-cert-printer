const INITIAL_STATE = {
    courses: [],
    error: false,
    message: {
        message: '',
        errors: {}
    },
    created: false,
    edit: {
        loaded: false,
        course: {
            certs: []
        }
    }
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'GET_COURSES':
            return {...state, courses: action.payload}
        case 'COURSE_CREATE':
            return {
                ...state,
                error: action.payload.error,
                message: action.payload.message,
                created: action.payload.created
            }
        case 'GET_COURSE_EDIT':
            return {...state, edit: action.payload}
        case 'EDIT_COURSE':
            return {
                ...state,
                error: action.payload.error,
                message: action.payload.message,
                created: action.payload.created
            }
        case 'CLEANUP_COURSE':
            return action.payload
        default:
            return state
    }
}
