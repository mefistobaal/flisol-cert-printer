import React from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios'
import Swal from "sweetalert2";

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isValidating: false,
            finded: false,
            data: []
        };
        this.submitConsult = this.submitConsult.bind(this);
        localStorage.setItem('userPath', '/home');
    }

    submitConsult(event) {
        event.preventDefault();
        this.setState({isValidating: true});
        Swal.showLoading()
        axios.post('/api/certificado', new FormData(event.target)).then(response => {
            if (response.status === 200)
                return response.data
        }).then(data => {
            this.setState({isValidating: false});
            if (data.length > 0) {
                this.setState({finded: true, data: data[0]})
                Swal.close()
            } else {
                Swal.fire('Oops', 'No existe el certificado', 'warning').then(() => this.setState({
                    finded: false,
                    data: []
                }))
            }
        }).catch(e => {
            this.setState({isValidating: false})
            Swal.fire('Opps', 'Intentalo mas tarde', 'error')
        })
        // fetch('/api/consulta/certificado', {
        //     method: 'POST',
        //     body:
        // })
    }

    render() {
        const {isValidating, finded, data} = this.state;
        return (
            <div className="page-wrapper">
                <div className="page-content--bge5">
                    <div className="container">
                        <div className="login-wrap">
                            <div className="login-content">
                                <div className="login-logo align-content-center">
                                    <img src={this.props.logo} alt="minTIC"/>
                                </div>
                                <div className="login-form">
                                    <p>Consulta tu certificado de asistencia</p>
                                    <form onSubmit={this.submitConsult}>
                                        <div className="form-group">
                                            <input className="au-input au-input--full" type="text" name="document"
                                                   required disabled={isValidating}/>
                                        </div>
                                        <button className="au-btn au-btn--block au-btn--green m-b-20" type="submit"
                                                disabled={isValidating}>Consultar
                                        </button>
                                        <div className="social-login-content">
                                            <div className="social-button">
                                                {finded ? (
                                                    <div className="card border border-success">
                                                        <div className="card-header"><strong
                                                            className="card-title">{data.first_names + ' ' + data.second_names}</strong>
                                                        </div>
                                                        <div className="card-body">
                                                            <p className="card-text">
                                                                <strong>Correo:</strong> {data.email}
                                                            </p>
                                                            <p className="card-text">
                                                                <strong>Documento:</strong> {data.doc_type + ' ' + data.document}
                                                            </p>
                                                            {data.certs.map(cert => (
                                                                <a href={'/pdf/' + cert.verification_code}
                                                                   target="_blank"><i
                                                                    className="fa fa-download"/> {cert.cert?.cert_title}
                                                                </a>
                                                            ))}
                                                        </div>
                                                    </div>
                                                ) : null}
                                                {/*<Link to="/verificar">*/}
                                                {/*    <button type="button"*/}
                                                {/*            className="au-btn au-btn--block au-btn--blue m-b-20">Validar*/}
                                                {/*        mi certificado*/}
                                                {/*    </button>*/}
                                                {/*</Link>*/}
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
