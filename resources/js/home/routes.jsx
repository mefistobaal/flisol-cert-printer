import React from 'react'
import {Redirect, Route, Switch} from "react-router-dom";
import {MemoryRouter as Router} from "react-router";
import Home from "./index";
import VerificarCertificado from "./verificar";
import axios from "axios";

export default class HomeRoutes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            actualPath: localStorage.getItem('userPath'),
            logo: 'images/logo_footer_radiofest.svg'
        }
    }

    componentDidMount() {
        axios.get('/api/config').then(data => {
            this.setState({logo: data.data.home_logo ?? 'images/logo_footer_radiofest.svg'})
        })
    }

    render() {
        return (
            <Router>
                <Switch>
                    <Route path="/" exact>
                        {!this.state.actualPath
                            ? <Home/>
                            : <Redirect to={this.state.actualPath}/>
                        }
                    </Route>
                    <Route path="/home">
                        <Home logo={this.state.logo}/>
                    </Route>
                    <Route path="/verificar">
                        <VerificarCertificado logo={this.state.logo}/>
                    </Route>
                </Switch>
            </Router>
        )
    }
}
