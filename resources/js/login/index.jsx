import React, {useEffect, useState} from 'react';
import axios from 'axios'
import Swal from "sweetalert2";

export const Login = () => {
    const token = sessionStorage.getItem('certToken')
    const [logo, setLogo] = useState('images/logo_footer_radiofest.svg')
    if (token !== null) {
        location.replace('/admin')
    }
    useEffect(() => {
        axios.get('/api/config').then(data => data.data.home_logo ? setLogo(data.data.home_logo) : null)
    }, [])
    const [isLogging, setIsLogging] = useState(false)
    const submit = async event => {
        event.preventDefault()
        setIsLogging(true)
        Swal.showLoading()
        const Toast = Swal.mixin({
            toast: true,
            position: 'bottom-right',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: false,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer);
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });
        try {
            const {data} = await axios.post('/api/login', new FormData(event.target));
            await sessionStorage.setItem('certToken', 'Bearer ' + data);
            location.replace('/admin')
        } catch (e) {
            setIsLogging(false)
            await Toast.fire({
                icon: 'error',
                title: 'Usuario no encontrado'
            })
        }
    }

    return (
        <div className="page-wrapper">
            <div className="page-content--bge5">
                <div className="container">
                    <div className="login-wrap">
                        <div className="login-content">
                            <div className="login-logo align-content-center">
                                <img src={logo} alt="MinTIC"/>
                            </div>
                            <div className="login-form">
                                <p>Inicio de sesión administrativo</p>
                                <form onSubmit={submit}>
                                    <div className="form-group">
                                        <input className="au-input au-input--full" type="email" name="email"
                                               required disabled={isLogging} placeholder="Email"/>
                                    </div>

                                    <div className="form-group">
                                        <input className="au-input au-input--full" type="password" name="password"
                                               required disabled={isLogging} placeholder="Contraseña"/>
                                    </div>
                                    <button className="au-btn au-btn--block au-btn--submit m-b-20" type="submit"
                                            disabled={isLogging}>Ingresar
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
