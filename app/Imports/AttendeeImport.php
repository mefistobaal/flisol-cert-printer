<?php

namespace App\Imports;

use App\Models\Attendee;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;

class AttendeeImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return Model|Attendee|Bool
     */
    public function model(array $row)
    {
        $email = $this->cleanEmail($row[4]);
        if (false !== filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $status       = strtolower(trim($row[6]));
            $attendeeData = [
                'first_names'  => ucfirst(trim($row[0])),
                'second_names' => ucfirst(trim($row[1])),
                'doc_type'     => strtoupper(trim($row[2])),
                'document'     => trim($row[3]),
                'email'        => strtolower(trim($row[4])),
                'phone'        => trim($row[5]),
                'status'       => !(($status == 'false')) && !(($status == '0'))
            ];
            if ($attendee = Attendee::withTrashed()->where('document', $row[3])->first()) {
                $attendee->update($attendeeData);
                return $attendee;
            } else {
                return new Attendee($attendeeData);
            }
        }
    }

    private function cleanEmail(string $email): string
    {
        $email      = explode(' ', $email);
        array_walk($email, function (&$value){
            $value = trim(strtolower($value));
        });
        return implode('', $email);
    }
}
