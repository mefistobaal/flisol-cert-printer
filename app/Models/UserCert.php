<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCert extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'user_certs';
    protected $fillable = ['attendee_id', 'cert_id', 'rol_id', 'verification_code'];
    protected $with = ['cert'];

    public function cert()
    {
        return $this->belongsTo(Cert::class, 'cert_id');
    }

    public function attendee()
    {
        return $this->belongsTo(Attendee::class, 'attendee_id');
    }
}
