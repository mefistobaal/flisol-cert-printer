<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attendee extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'attendee';
    protected $fillable = ['first_names', 'second_names', 'doc_type', 'document', 'email', 'phone', 'status'];
    protected $with = ['certs'];

    public function certs()
    {
        return $this->hasMany(UserCert::class);
    }
}
