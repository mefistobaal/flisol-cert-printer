<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['name', 'status'];
    protected $table = 'course';
//    protected $with = ['certs'];
    use HasFactory;

    public function certs()
    {
        return $this->hasMany(CourseCert::class);
    }
}
