<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseCert extends Model
{
    use HasFactory;

    protected $table = 'course_certs';
    protected $fillable = ['cert_id', 'course_id'];

//    protected $with = ['cert'];

    public function cert()
    {
        return $this->belongsTo(Cert::class);
    }
}
