<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemConfig extends Model
{
    protected $table = 'system_configs';
    use HasFactory;

    public function files()
    {
        return $this->belongsToMany(File::class);
    }
}
