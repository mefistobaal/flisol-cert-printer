<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cert extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'cert';
    protected $fillable = ['cert_title', 'celebrated_on', 'year', 'cert_title_color', 'year_color', 'name_color', 'cert_image_path'];
    protected $withCount = ['attendees'];

    public function rol()
    {
        return $this->hasOne(Rol::class);
    }

    public function course()
    {
        return $this->hasOne(CourseCert::class);
    }

    public function attendees()
    {
        return $this->hasMany(UserCert::class);
    }
}
