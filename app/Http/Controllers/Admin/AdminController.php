<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\SystemController;
use App\Http\Requests\Admin\AdminCreate;
use App\Http\Requests\Admin\AdminEdit;
use App\Models\Admin;
use App\Models\SystemConfig;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function admin()
    {
        $favicon = new SystemController();
        return view('admin', ['title_site' => SystemConfig::find(1)->title_site, 'favicon' => $favicon->system()->favicon]);
    }

    public function login()
    {
        $favicon = new SystemController();
        return view('auth.login', ['title_site' => SystemConfig::find(1)->title_site, 'favicon' => $favicon->system()->favicon]);
    }

    public function adminData()
    {
        return Auth::user();
    }

    public function create(AdminCreate $adminCreate)
    {
        $adminCreate->validated();
        return Admin::create([
            'name'     => $adminCreate->input('name'),
            'email'    => $adminCreate->input('email'),
            'password' => Hash::make($adminCreate->input('password')),
            'status'   => $adminCreate->input('status')
        ]);
    }

    public function update(AdminEdit $request)
    {
        $admin           = Admin::find($request->input('id'));
        $admin->name     = $request->input('name');
        $admin->email    = $request->input('email');
        $admin->password = $request->input('password') ? Hash::make($request->input('password')) : $admin->password;
        $admin->status   = $request->input('status');
        $admin->save();
        return $admin;
    }

    public function delete($id)
    {
        return Admin::destroy($id);
    }
}
