<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminLogin;
use App\Models\LoginHistory;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Jenssegers\Agent\Facades\Agent;


class SessionController extends Controller
{
    public function login(AdminLogin $request)
    {
        $request->validated();

        $user = User::where('email', $request->input('email'))->where('status', 1)->first();

        if (!$user or !Hash::check($request->input('password'), $user->password)) {
            throw ValidationException::withMessages([
                'email' => [trans('auth.failed')]
            ]);
        }

        LoginHistory::create([
            'admin_id'    => $user->id,
            'ip'          => $request->ip(),
            'remote_host' => $request->getHttpHost(),
            'so'          => Agent::platform(),
            'navigator'   => Agent::browser(),
        ]);

        return $user->createToken('Auth Token')->accessToken;
    }
}
