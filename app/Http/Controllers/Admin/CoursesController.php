<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CoursesCreate;
use App\Http\Requests\CoursesEdit;
use App\Models\Course;

class CoursesController extends Controller
{
    public function create(CoursesCreate $create)
    {
        $create->validated();
        return Course::create($create->all());
    }

    public function update(CoursesEdit $edit)
    {
        return Course::find($edit->input('id'))->update($edit->all());
    }
}
