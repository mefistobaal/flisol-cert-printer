<?php

namespace App\Http\Controllers\Admin;

use App\Exports\AttendeeExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AttendeeCreate;
use App\Http\Requests\Admin\AttendeeImport;
use App\Http\Requests\Admin\AttendeeSearch;
use App\Http\Requests\Admin\AttendeeUpdate;
use App\Models\Attendee;
use App\Models\UserCert;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class AttendeeController extends Controller
{
    public function create(AttendeeCreate $create)
    {
        $create->validated();
        $attendee = Attendee::create($create->all());
        return $attendee;
    }

    public function update(AttendeeUpdate $update)
    {
        return Attendee::withTrashed()->find($update->input('id'))->update($update->all());
//        $attendee = Attendee::withTrashed()->find($update->input('id'));
//        if (!$attendee->status) {
//            $attendee->delete();
//        } else {
//            $attendee->restore();
//        }
//        return $attendee;
//        return $update->all();
    }

    public function delete($id)
    {
        $attendee = Attendee::find($id);
        $attendee->status = false;
        $attendee->save();
        return Attendee::destroy($id);
    }

    public function restore($id){
        $attendee = Attendee::withTrashed()->find($id);
        $attendee->restore();
        return $attendee;
    }

    public function search(AttendeeSearch $search)
    {
        $document = $search->input('document');
        $doc_type = $search->input('doc_type') == 'selected' ? null : $search->doc_type;
        if (!is_null($document) && !is_null($doc_type)) {
            return Attendee::withTrashed()->where('document', 'like', "%$document%")->where('doc_type', '=', $doc_type)->paginate();
        } else if (!is_null($document) && is_null($doc_type)) {
            return Attendee::withTrashed()->where('document', 'like', "%$document%")->paginate();
        } elseif (is_null($document) && !is_null($doc_type)) {
            return Attendee::withTrashed()->where('doc_type', '=', $doc_type)->paginate();
        } else {
            return Attendee::paginate();
        }
    }

    public function import(AttendeeImport $import)
    {
        Excel::import(new \App\Imports\AttendeeImport, $import->file('excel')->store('temp'));
        return response()->json(['message' => 'Importado'], 201);
    }

    public function export()
    {
        $date = Carbon::now();
        return Excel::download(new AttendeeExport, "asistentes-$date.csv");
    }

    public function certs(Request $request)
    {
        $actualCerts = UserCert::withTrashed()->where('attendee_id', $request->input('attendee_id'))->get();
        if (empty($request->input('certs'))) {
            foreach ($actualCerts as $cert) {
                UserCert::destroy($cert->id);
            }
            return response()->json(['message' => 'Deleted all certs']);
        }
        foreach ($request->input('certs') as $item) {
            if ($possible = UserCert::withTrashed()->where('attendee_id', $request->input('attendee_id'))->where('cert_id', $item)->first()) {
                if ($possible->trashed()) {
                    if ($possible->trashed()) {
                        $possible->restore();
                    }
                }
            } else {
                UserCert::create([
                    'attendee_id'       => $request->input('attendee_id'),
                    'cert_id'           => $item,
                    'rol_id'            => 1,
                    'verification_code' => Str::uuid()
                ]);
            }
        }
        $certsToDelete = UserCert::withoutTrashed()->where('attendee_id', $request->input('attendee_id'))->whereNotIn('cert_id', $request->input('certs'))->get();
        foreach ($certsToDelete as $toDelete) {
            UserCert::destroy($toDelete->id);
        }
    }
}
