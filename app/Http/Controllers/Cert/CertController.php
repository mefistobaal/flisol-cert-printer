<?php

namespace App\Http\Controllers\Cert;

use App\Http\Controllers\Controller;
use App\Http\Requests\Cert\CreateCert;
use App\Http\Requests\CertAttendeeAssign;
use App\Models\Attendee;
use App\Models\Cert;
use App\Models\CourseCert;
use App\Models\UserCert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CertController extends Controller
{
    public function create(CreateCert $cert)
    {
        $cert->validated();
        $newCert = Cert::create($cert->input());
        CourseCert::create([
            'cert_id'   => $newCert->id,
            'course_id' => $cert->input('course'),
        ]);
        if ($cert->input('backgroundData')) {
            $file                     = $this->saveBackgroundCertImage($cert->input('backgroundData'));
            $newCert->cert_image_path = $file;
            $newCert->save();
        }
        return $newCert;
    }

    private function saveBackgroundCertImage($background): string
    {
        $extension = explode('/', explode(':', substr($background, 0, strpos($background, ';')))[1])[1];
        $replace   = substr($background, 0, strpos($background, ',') + 1);
        $file      = str_replace($replace, '', $background);
        $file      = str_replace(' ', '+', $file);
        $file      = base64_decode($file);
        $certPath  = 'certs/backgrounds/' . Str::uuid() . '.' . $extension;
        Storage::disk('public')->put($certPath, $file);
        return $certPath;
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        return Cert::where('cert_title', 'like', "%$search%")->paginate();
    }

    public function update(CreateCert $cert)
    {
        $course = CourseCert::where('cert_id', $cert->input('id'))->first();
        if (!$course) {
            CourseCert::create([
                'cert_id'   => $cert->input('id'),
                'course_id' => $cert->input('course')
            ]);
        } else {
            $course->update(['course_id' => $cert->input('course')]);
        }
        $certToUpdate = Cert::find($cert->input('id'));
        if ($cert->input('backgroundData')) {
            $file                          = $this->saveBackgroundCertImage($cert->input('backgroundData'));
            $certToUpdate->cert_image_path = $file;
            $certToUpdate->save();
        }
        $certToUpdate->update($cert->all());
        return $certToUpdate;
    }

    public function delete($id)
    {
        return Cert::destroy($id);
    }

    public function assign(CertAttendeeAssign $assign)
    {
        $documents_not_found = [];
        $documents           = explode(',', $assign->input('attendeesList'));
        array_walk($documents, function (&$value) {
            $value = trim($value);
        });
        foreach ($documents as $document) {
            $attendee = Attendee::where('document', $document)->first();
            if ($attendee) {
                $userCert = UserCert::where('attendee_id', $attendee->id)->where('cert_id', $assign->input('cert_id'))->first();
                if (!$userCert) {
                    UserCert::create([
                        'attendee_id'       => $attendee->id,
                        'cert_id'           => $assign->input('cert_id'),
                        'rol_id'            => 1,
                        'verification_code' => Str::uuid()
                    ]);
                }
            } else {
                array_push($documents_not_found, $document);
            }
        }
        return $documents_not_found;
    }
}
