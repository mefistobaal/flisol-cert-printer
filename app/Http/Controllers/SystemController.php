<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\UpdateSystemConfig;
use App\Models\File;
use App\Models\SystemConfig;
use Illuminate\Support\Facades\Storage;

class SystemController extends Controller
{
    public function system()
    {
        $system             = SystemConfig::find(1);
        $system->admin_logo = $system->admin_logo ? Storage::url(File::find($system->admin_logo)->route) : null;
        $system->home_logo  = $system->home_logo ? Storage::url(File::find($system->home_logo)->route) : null;
        $system->favicon    = $system->favicon ? Storage::url(File::find($system->favicon)->route) : null;
        return $system;
//        $system->favicon =
//        return Storage::url('favicons/351-x-563.png');
    }

    public function update(UpdateSystemConfig $updateSystemConfig)
    {
        $system             = SystemConfig::find(1);
        $system->title_site = $updateSystemConfig->input('site_title');
        if ($updateSystemConfig->hasFile('favicon') && $updateSystemConfig->file('favicon')->isValid()) {
            $path            = $updateSystemConfig->file('favicon')->storePublicly('favicons', 'local');
            $newFavicon      = File::create([
                'name'  => $updateSystemConfig->file('favicon')->getClientOriginalName(),
                'route' => $path
            ]);
            $system->favicon = $newFavicon->id;
        }
        if ($updateSystemConfig->hasFile('admin_logo') && $updateSystemConfig->file('admin_logo')->isValid()) {
            $path               = $updateSystemConfig->file('admin_logo')->storePublicly('admin', 'local');
            $newAdmin           = File::create([
                'name'  => $updateSystemConfig->file('admin_logo')->getClientOriginalName(),
                'route' => $path
            ]);
            $system->admin_logo = $newAdmin->id;
        }
        if ($updateSystemConfig->hasFile('home_logo') && $updateSystemConfig->file('home_logo')->isValid()) {
            $path              = $updateSystemConfig->file('home_logo')->storePublicly('home', 'local');
            $newHome           = File::create([
                'name'  => $updateSystemConfig->file('home_logo')->getClientOriginalName(),
                'route' => $path
            ]);
            $system->home_logo = $newHome->id;
        }
        $system->save();
        return $system;
    }
}
