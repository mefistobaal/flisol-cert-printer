<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Attendee;
use App\Models\Stats;
use App\Models\UserCert;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;


class PDFController extends Controller
{
    public function make($id)
    {
        $cert     = UserCert::where('verification_code', $id)->first();
        $attendee = Attendee::find($cert->attendee_id);
        $name     = mb_strtoupper(trim($attendee->first_names) . ' ' . trim($attendee->second_names), 'UTF-8');
        if (count(str_split($name)) > 25) {
            $name = mb_strtoupper(trim($attendee->first_names) . ' ' . explode(' ', trim($attendee->second_names))[0], 'UTF-8');
        }
        Carbon::setLocale('es');
        $data = [
            'title'         => $cert->cert->cert_title,
            'titleColor'    => $cert->cert->cert_title_color,
            'certImagePath' => $cert->cert->cert_image_path ? 'storage/public/' . $cert->cert->cert_image_path : 'images/DIPLOMA_MINTIC.png',
            'name'          => $name,
            'document'      => strtoupper($attendee->doc_type . ' ' . $attendee->document),
            'nameColor'     => $cert->cert->name_color,
            'dateColor'     => $cert->cert->year_color,
            'date'          => Carbon::createFromDate($cert->cert->year)->toDateString()
        ];
        $pdf              = PDF::loadView('cert', $data)->setPaper('letter', 'landscape');
        $stats            = Stats::find(1);
        $stats->certs_gen += 1;
        $stats->save();
        return $pdf->stream('RadioFest' . $attendee->document . '.pdf');
    }
}
