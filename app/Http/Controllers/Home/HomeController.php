<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Attendee;
use App\Models\Stats;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function query(Request $request)
    {
        $stats             = Stats::find(1);
        $stats->querys_gen += 1;
        $stats->save();
        $attendees = Attendee::where('email', $request->document)->orWhere('document', $request->document)->get()->load('certs');
        return $attendees->reject(function ($attendee) {
            return !$attendee->status;
        });
    }

    public function stats()
    {
        return ['certs' => Stats::find(1)->first(), 'users' => Attendee::all()->count()];
    }
}
