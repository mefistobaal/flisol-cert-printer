<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AttendeeCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_names'  => ['required', 'string'],
            'second_names' => ['required', 'string'],
            'doc_type'     => ['required', Rule::in(['CC', 'Passport', 'TI', 'NIT'])],
            'document'     => ['required', 'unique:attendee,document'],
            'email'        => ['required', 'email'],
            'phone'        => ['required', 'string']
        ];
    }
}
