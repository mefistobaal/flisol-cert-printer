<?php

namespace App\Http\Requests\Cert;

use Illuminate\Foundation\Http\FormRequest;

class CreateCert extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cert_title'       => ['required', 'string'],
            'cert_title_color' => ['required', 'string'],
            'celebrated_on'    => ['required', 'string'],
            'name_color'       => ['required', 'string'],
            'year'             => ['required'],
            'year_color'       => ['required']
        ];
    }
}
